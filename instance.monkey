#Rem
DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Preprocessor related:
'#REFLECTION_FILTER = "*"

' Imports (Public):
Import regalnet

'Import byteorder

' BRL:
Import brl.databuffer
Import brl.datastream
Import brl.pool

' Imports (Private):
Private

Import external
Import encryption

Public

' Constants:

' Encryption related:
Const RN_ENCRYPT_DISABLED:Int 	= RN_Instance.ENCRYPT_DISABLED
Const RN_ENCRYPT_AES:Int 		= RN_Instance.ENCRYPT_AES ' Not supported yet.
Const RN_ENCRYPT_RC4:Int 		= RN_Instance.ENCRYPT_RC4

' Functions:
Function RN_Init:Bool()
	Return RN_Instance.Init()
End

Function RN_Deinit:Bool()
	Return RN_Instance.Deinit()
End

' Classes:
#If Not RN_COMMUNITY_SOCKETS

' Since 'brl.socket' wants me to implement these interfaces (For the 'async' functionality), I need this line.
Class RN_Instance Implements RN_DESTINATIONS, IOnReceiveComplete, IOnReceiveFromComplete, IOnConnectComplete, IOnSendComplete, IOnSendToComplete Final

#Else

' Here's the normal beginning of the instance class:
Class RN_Instance Implements RN_DESTINATIONS Final

#End
	' Constant variables:
	
	' A rough estimate of the internal header size.
	Const HeaderSize:Int = 24
	
	' A basic error string used for debugging and things of that nature.
	Const ErrorStr:String = "RegalNET - Error(RN_Instance): "
	
	' Defaults:
	
	' The maximum number of packets we can deal with at a time.
	Const Default_MaxPacketsPerCycle:Int = 64 ' 128
	
	' Flags / Booleans:
	' Nothing so far.
	
	' Encryption methods:
	Const ENCRYPT_DISABLED:Int 	= 0
	Const ENCRYPT_AES:Int 		= 1
	Const ENCRYPT_RC4:Int 		= 2
	
	' These don't really matter:
	Const Yes:Bool	= True
	Const No:Bool	= False
	
	' Global variables:
	
	' A list of all RegalNET instances.
	'Global List:List<RN_Instance> = Null
	
	' The password we normally use to encrypt information.
	Global EncryptionPassword:String = "Password"
	
	' Defaults:
	Global Default_BufferSize:Int = 2048
	
	' Flags / Booleans:
	Global Default_FixByteOrder:Bool = True
	
	' Functions:
	Function Init:Bool()
		#If QSOCK_ENABLED
			Return QSocket.Init()
		#Else
			' Nothing so far.
		#End
		
		' Return the default response.
		Return True
	End
	
	Function Deinit:Bool()
		#If QSOCK_ENABLED
			Return QSocket.Deinit()
		#Else
			' Nothing so far.
		#End
		
		' Return the default response.
		Return True
	End
	
	Function Convert_IntToString_IP:String(IP:Int)
		#If QSOCK_ENABLED
			Return QSocket.IntToStringIP(IP)
		#Else
			Local Oct:Int[4]
			
			For Local Index:= 0 Until 4
				Oct[Index] = (IP Shr ((3-(Index))*8))
				
				If (Index > 0) Then
					Oct[Index] = Oct[Index] & 255
				Endif
			Next
			
			Return String(Oct[0]) + "." + String(Oct[1]) + "." + String(Oct[2]) + "." + String(Oct[3])
		#End
	End Function
	
	Function Convert_StringToInt_IP:Int(IP:String)
		#If QSOCK_ENABLED
			Return QSocket.StringToIntIP(IP)
		#Else
			Local IntIP:Int = 0
			Local Oct:Int[4]
			Local TempStr:String = ""
			
			Oct[0] = Int(retrostrings.Left(IP, retrostrings.Instr(IP, ".")-1))
			TempStr = retrostrings.Right(IP, IP.Length()-retrostrings.Instr(IP, "."))
			Oct[1] = Int(retrostrings.Left(TempStr, retrostrings.Instr(TempStr, ".")-1))
			TempStr = retrostrings.Right(TempStr, TempStr.Length() - retrostrings.Instr(TempStr, "."))
			Oct[2] = Int(retrostrings.Left(TempStr, retrostrings.Instr(TempStr, ".")-1))
			TempStr = retrostrings.Right(TempStr, TempStr.Length() - retrostrings.Instr(TempStr, "."))
			Oct[3] = Int(TempStr)
			TempStr = ""
			
			For Local I:= 0 Until 4
				IntIP += (Oct[I] Shl (24-(8*I)))
			Next
		
			Return IntIP
		#End
		
		' If by some off chance we need this, return zero.
		Return 0
	End Function
		
	' Constructor(s):
	Method New(BufferSize:Int=Default_BufferSize, MessageTimeout:Int=250, ConnectionTimeout:Int=10000, MaxPacketsPerCycle:Int=Default_MaxPacketsPerCycle, PongLifetime:Int=-1, FixByteOrder:Bool=Default_FixByteOrder)
		Construct(BufferSize, Null, MessageTimeout, ConnectionTimeout, MaxPacketsPerCycle, PongLifetime, FixByteOrder)
	End
	
	Method New(BufferSize:Int, MessageHandler:RN_MessageHandler, MessageTimeout:Int=250, ConnectionTimeout:Int=10000, MaxPacketsPerCycle:Int=Default_MaxPacketsPerCycle, PongLifetime:Int=-1, FixByteOrder:Bool=Default_FixByteOrder)
		Construct(BufferSize, MessageHandler, MessageTimeout, ConnectionTimeout, MaxPacketsPerCycle, PongLifetime, FixByteOrder)
	End
	
	' Constructor(s) (Private):
	Private
	
	Method Construct:Void(BufferSize:Int=Default_BufferSize, MessageHandler:RN_MessageHandler=Null, MessageTimeout:Int=250, ConnectionTimeout:Int=10000, MaxPacketsPerCycle:Int=Default_MaxPacketsPerCycle, PongLifetime:Int=-1, FixByteOrder:Bool=Default_FixByteOrder)
		#Rem
		If (RN_Instance.List = Null) Then
			RN_Instance.List = New List<RN_Instance>()
		Endif
	
		RN_Instance.List.AddLast(Self)
		#End
		
		If (PongLifetime = -1) Then
			PongLifetime = ConnectionTimeout * 10
		Endif
		
		#If CONFIG = "debug"
			Self.DebugPause = True
			Self.AlertOnError = Self.DebugPause
			Self.ReportBlanks = True
		#Else
			Self.DebugPause = False
			Self.AlertOnError = True
			Self.ReportBlanks = False
		#Endif
		
		Self.IsClosed = True
		Self.ConnectionPassword = "Password"
		
		Self.BufferSize = BufferSize + HeaderSize ' The extra bytes are for header/meta-data related systems.
		Self.FixByteOrder = FixByteOrder
		
		#If QSOCK_ENABLED
			'If (RN_Instance.List.Count() = 1) Then
			'QSocket.Init()
			'Endif
			
			' The extra bytes are for the header (There are more added than needed; this is because of the password and addresses).
			Self.Socket = New QSocket(Self.BufferSize, Self.FixByteOrder)
		#Elseif TARGET = "" ' Anything that doesn't need BufferSize can be here.
			' Nothing so far.
		#Else
			#If Not RN_HLINTERNALS
				' Nothing so far. This situation shouldn't really be reached anyway.
			#End
		
			' Everything that uses the QuickSock-style 'emulation' layer:
			#If RN_HLINTERNALS
				Self.Socket = Null
			#End
			
			#If ANET_ENABLED
				Self.Socket = New Socket()
				Self.Socket.CreateUDPSocket()
			#End
			
			#If Not RN_COMMUNITY_SOCKETS
				Self.Socket = New Socket("datagram")
			#End
			
			#If RN_HLINTERNALS
				'#If Not RN_COMMUNITY_SOCKETS Or ANET_ENABLED
				Self.OutData = New DataBuffer(Self.BufferSize)
				Self.OutStream = New DataStream(Self.OutData)
				'#End
			#End			
		#Endif
		
		Self.MaxPacketsPerCycle = MaxPacketsPerCycle
		
		If (MessageHandler <> Null) Then
			Self.MessageHandler = MessageHandler
		Else
			Self.MessageHandler = New RN_MessageHandler(Self, MessageTimeout, PongLifetime, ConnectionTimeout)
		Endif
		
		' Generate the intermediate cache.
		GenerateIntermediateCache()
		
		#If RN_HLINTERNALS
			Self.HL_MessageAvail = False
		#End
		
		#If Not RN_COMMUNITY_SOCKETS
			CachedSocketAddresses = New List<SocketAddress>()
		#End
	End
	
	' Cache-related methods:
	
	#Rem
		INTERMEDIATE CACHE NOTES:
			* Caches can be used externally, but are not safe when used with some RegalNET commands.
			Caches are mainly meant as tools for transferring data off of and onto packet-streams.
			
			That being said, raw bulk data-reading through 'RN_Instance' uses the cache system.
			In general, if RegalNET produces a somewhat large integer array at any point, it may be from a cache.
			
			Some, but not all routines which use caches within RegalNET have options to not request a standard cache.
			These commands DO NOT make a commitment to using the cache to begin with, but they do guarantee the cache WON'T be used.
			
			* You should under no cercumstances expect the size of the cache-array to be the exact size you requested.
			That being said, this is referring to the size being larger, not smaller.
			The size you requested will always be a minimum size for the output.
			
			TL;DR: DO NOT use the 'Length' property of a cache.
			
			* Caches are used by many things, and unless requested to be cleared, they will likely contain garbage.
			
			* If at some point this becomes multi-threaded, caches may be produced through some form of pool, and passed around from there.
			
			* You should NEVER rely on your data sticking to a cache between routines which work with RegalNET directly.
			Caches should only be used for TEMPORARY data storage,
			such as internal use within RegalNET, intermediate data storage for streams,
			or a return-array for a 'low-profile' access routine.
			
			Caches are not reserved, they are requested, RegalNET could at any time use the cache you requested.
			For this reason, caches generally shouldn't be used with non-cache-related RegalNET commands.
			
			DO NOT USE CACHES FOR LONG-TERM DATA. Such situations should either involve copying, or using your own arrays/caches.
			
			* For the sake of keeping functionality open, some cache management commands are public, despite their internal nature.
			These commands are perfectly normal, and can be used as you see fit.
			
			* Unless you're generating your own cache, a standard cache should be used.
			This is done by calling 'RequestIntermediateCache', which will produce an internal cache from RegalNET.
			
			* As per usual, do not try to "destruct" a requested cache. (This is already done for you)
			
			* By generating a cache manually through RegalNET, you are disclosing that your data may at any point be READ by RegalNET.
			RegalNET does not have the right to WRITE to any custom caches, without first the given command disclosing such information (Either by name, or by documentation).
			But really, you should just make your own integer array normally at that point.
			Functionality for managing custom caches is not yet implemented, and therefore rather basic.
			
			As a word of advice, stay away from custom cache creation for now, it's not really a thing yet.
	#End
	
	Method GenerateIntermediateCache:Int[]()
		Return GenerateIntermediateCache(Self.BufferSize)
	End
	
	Method GenerateIntermediateCache:Int[](RequestedCacheSize:Int, ManageSize:Bool=True)
		' Process the current cache.
		Intermediate_Cache = GenerateIntermediateCache(Intermediate_Cache, RequestedCacheSize, ManageSize)
		
		' Return the processed cache.
		Return Intermediate_Cache
	End
	
	Public
	
	' Methods (Public):
	Method SetPassword:Void(Password:String="")
		Self.ConnectionPassword = Encrypt(Password)
	End
	
	Method Host:Bool(Port:Int=0, Encryption:Int=ENCRYPT_DISABLED, FullPortRange:Bool=False)
		' Local/Temporary variables:
		Local BindResponse:Bool = False
	
		If (Self.InitializedOnce) Then
			Disconnect()
		Endif
			
		If (Self.Socket <> Null) Then
			If (Port = 0) Then
				Local LastSeed:Int = Seed
			
				Seed = Millisecs()
				
				If (FullPortRange) Then
					' The first 1024 are always ignored.
					Port = Rnd(1024, Pow(2,16)-1)
				Else
					' This is the more 'proper' way of doing this.
					Port = Rnd(49152, Pow(2, 16)-1)
				Endif
				
				Seed = LastSeed
			Endif
		
			#If QSOCK_ENABLED
				BindResponse = Self.Socket.Host(Port)
			#Elseif ANET_ENABLED
				BindResponse = Self.Socket.BindSocket(Port)
			#Elseif Not RN_COMMUNITY_SOCKETS
				BindResponse = Self.Socket.Bind("", Port)
				
				If (Self.Socket.IsBound) Then
					Self.ExternalAddress = New SocketAddress("", 0)
					'Self.Socket.ReceiveFromAsync(New DataBuffer(Self.BufferSize), 0, Self.BufferSize, Self.ExternalAddress, Self)
					
					BRL_GenerateBuffers()
					
					Self.BRL_MessageFound = False
					Self.RelaunchReceiver = True
				Endif
			#Else
				' Nothing so far.
			#End
			
			Self.IsClosed = False
			Self.IsServer = True
			Self.IPort = Port
			Self.EPort = Port
			Self.Encryption = Encryption
			
			If (Not Self.InitializedOnce) Then
				Self.InitializedOnce = BindResponse
			Endif
			
			If (Not BindResponse) Then				
				If (Not Self.DebugSet Or Self.AlertOnError) Then
					Local ErrorData:String
					
					ErrorData = String(ErrorStr + "Host(): Unable to bind the internal socket to port (" + String(Port) + ") with any address.")
					
					If (Self.DebugPause) Then
						Print(ErrorData)
						
						DebugStop()
					Else
						Error(ErrorData)
					Endif
				Endif
				
				Self.IsClosed = True
				
				Return False
			Endif
		Endif
	
		Return BindResponse
	End
	
	Method Connect:Bool(IP:Int, ExternalPort:Int, InternalPort:Int=0, Encryption:Int=ENCRYPT_DISABLED, FullPortRange:Bool=False)
		Return Connect(Convert_IntToString_IP(IP), ExternalPort, InternalPort, Encryption)
	End
	
	Method Connect:Bool(IP:String, ExternalPort:Int, InternalPort:Int=0, Encryption:Int=ENCRYPT_DISABLED, FullPortRange:Bool=False)
		' Local variable declarations:
		Local Response:Bool = False
	
		If (Self.InitializedOnce) Then
			Disconnect()
		Endif
	
		If (Self.Socket <> Null) Then
			If (InternalPort = 0) Then
				Local LastSeed:Int = Seed
			
				Seed = Millisecs()
				
				If (FullPortRange) Then
					' The first 1024 are always ignored.
					InternalPort = Rnd(1024, Pow(2,16)-1)
				Else
					' This is the more 'proper' way of doing this.
					InternalPort = Rnd(49152, Pow(2, 16)-1)
				Endif
				
				Seed = LastSeed
			Endif
						
			#If QSOCK_ENABLED
				Response = Socket.Connect(IP, ExternalPort, InternalPort)
			#Elseif ANET_ENABLED
				Response = Self.Socket.BindSocket(InternalPort)
				
				If (Response) Then
					' Once we've bound the socket, connect to the remote server.
					Response = Self.Socket.ConnectSocket(IP, ExternalPort)
				Endif
				
				Response = True
			#Elseif Not RN_COMMUNITY_SOCKETS
				'Response = Self.Socket.Bind("127.0.0.1", InternalPort)
				Self.Socket.Bind("", InternalPort)
				Response = Self.Socket.Connect(IP, ExternalPort)
				
				If (Response) Then
					'Self.Socket.Connect(IP, ExternalPort)
					
					BRL_GenerateBuffers()
					
					Self.BRL_MessageFound = False
					'Self.BRL_MF_Temp = Not Self.BRL_MessageFound
					Self.RelaunchReceiver = True
				Endif
				
				Self.ExternalAddress = GetCachedSocketAddress(IP, ExternalPort)
			#Else
				' Nothing so far.
			#End
			
			' Field assignments:
			Self.IsClosed = False
			Self.IsServer = False
			Self.IPort = InternalPort
			Self.EPort = ExternalPort
			Self.IP = IP
			Self.Encryption = Encryption
			Self.InitializedOnce = True
			
			If (Not Response) Then
				If (Not Self.DebugSet Or Self.AlertOnError) Then
					Local ErrorData:String
					
					ErrorData = String(ErrorStr + "Connect(): Unable to bind the internal socket to port (" + String(InternalPort) + ") with any address.")
					
					If (Self.DebugPause) Then
						Print(ErrorData)
						
						DebugStop()
					Else
						Error(ErrorData)
					Endif
				Endif
				
				Self.IsClosed = True
				
				Return False
			Endif
		
			If (Response And MessageHandler <> Null) Then
				' Write an initialization message.
				MessageHandler.WriteInitMsg(Self.IP, Self.EPort)
				
				' Generate a new connection object.
				New RN_Connection(Self.MessageHandler, Self.IP, Self.EPort, MessageHandler.ConnectionLife)
			Endif
			
			Return Response
		Endif
	
		Return False
	End
	
	#If Not RN_COMMUNITY_SOCKETS
		Method BRL_GenerateBuffers:Void()
			Self.BRL_InData = ResizeBuffer(Self.BRL_InData, Self.BufferSize, True, True, True)
			Self.BRL_OutData = ResizeBuffer(Self.BRL_InData, Self.BufferSize, True, True, True)
			
			Return
		End
	#End
	
	Method FindConnection:RN_Connection(IP:Int, Port:Int)
		If (MessageHandler <> Null) Then
			Return MessageHandler.FindConnection(IP, Port)
		Endif
		
		' Return the default response.
		Return Null
	End
	
	Method FindConnection:RN_Connection(IP:String, Port:Int)
		If (MessageHandler <> Null) Then
			Return MessageHandler.FindConnection(IP, Port)
		Endif
		
		' Return the default response.
		Return Null
	End
	
	Method CheckUpdateErrors:Bool()
		' Simple checks that try to keep things from crashing:
		If (Self.IsClosed) Then Return True
		If (Self.Socket = Null) Then Return True
		If (Self.MessageHandler = Null) Then Return True
		
		' Platform specific checks:
		
		' BRL.Socket specific:
		#If Not RN_COMMUNITY_SOCKETS
			If (Not Self.Socket.IsBound) Then Return True
						
			If (Not Self.IsServer And Not Self.Socket.IsConnected) Then
				Return True
			Endif
		#End
	
		Return False
	End
	
	Method Update:Void(UpdateServices:Bool=False)
		#If Not RN_COMMUNITY_SOCKETS
			If (UpdateServices) Then
				UpdateAsyncEvents()
			Endif
		#End
	
		' Check for any errors that would stop us from updating.
		If (CheckUpdateErrors()) Then
			Return
		Endif
		
		' Update the internal message handler.
		MessageHandler.Update()
		
		' Return nothing (void).
		Return
	End
	
	Method Destroy:Bool()	
		If (Self.MessageHandler <> Null) Then
			Self.MessageHandler.Free()
		Endif
		
		#If RN_HLINTERNALS
			If (Self.InData <> Null) Then
				InData.Discard()
				
				Self.InData = Null
			Endif
			
			If (Self.OutData <> Null) Then
				OutData.Discard()
				
				Self.OutData = Null
			Endif
			
			If (Self.InStream) Then
				Self.InStream.Close()
				
				Self.InStream = Null
			Endif
			
			If (Self.OutStream) Then
				Self.OutStream.Close()
				
				Self.OutStream = Null
			Endif
			
			#If Not RN_COMMUNITY_SOCKETS
				If (Self.BRL_InData <> Null) Then
					Self.BRL_InData.Discard()
					
					Self.BRL_InData = Null
				Endif
				
				If (Self.BRL_OutData <> Null) Then
					Self.BRL_OutData.Discard()
					
					Self.BRL_OutData = Null
				Endif
			#End
		#Endif
		
		' Set the maximum packets per-cycle back to the default.
		MaxPacketsPerCycle = Default_MaxPacketsPerCycle
		
		' Sockets can be accessed by anyone.
		CloseSocket()
		
		' Free the internal socket.
		FreeSocket()

		' Set everything to null:
		Self.MessageHandler = Null		
		Self.Socket = Null

		Return True
	End
		
	Method Free:Bool()
		Local Response:Bool = False
	
		#Rem
		If (RN_Instance.List <> Null) Then
			RN_Instance.List.RemoveEach(Self)
		Endif
		#End
		
		Response = Destroy()
		
		#Rem
		#If QSOCK_ENABLED
			If (RN_Instance.List <> Null) Then
				If (RN_Instance.List.Count() = 0) Then QSocket.DeInit()
			Endif
		#End
		
		If (RN_Instance.List.IsEmpty()) Then
			RN_Instance.List.Clear()
			RN_Instance.List = Null
		Endif
		#End
	
		Return Response
	End
	
	Method ConfigureDebug:Void(Alert:Bool=True, DebugPause:Bool=True, ReportBlanks:Bool=True)
		Self.DebugSet = True
		Self.AlertOnError = Alert
		Self.DebugPause = DebugPause
		Self.ReportBlanks = ReportBlanks
		
		Return
	End
	
	Method Disconnect:Bool()
		CloseSocket()
		Flush()
		
		' Return the default response.
		Return Self.IsClosed
	End
	
	Method Flush:Void()
		If (Socket <> Null) Then
			#If ANET_ENABLED
				Socket.FlushStream()
			#End
		Endif
		
		If (MessageHandler <> Null) Then
			MessageHandler.Flush()
		Endif
		
		Return
	End
	
	Method FreeSocket:Bool()
		Local Response:Bool = False
	
		#If QSOCK_ENABLED
			Response = Self.Socket.Free()
		#Elseif ANET_ENABLED
			Response = True
		#Elseif Not RN_COMMUNITY_SOCKETS
			Response = True
		#Else
			' Nothing so far.
		#End
		
		Return Response
	End
	
	Method CloseSocket:Void()
		If (Socket <> Null) Then
			#If QSOCK_ENABLED
				Socket.Close()
			#Elseif ANET_ENABLED
				Socket.CloseSocket()
			#Elseif Not RN_COMMUNITY_SOCKETS
				Socket.Close()
			#Else
				' Nothing so far.
			#End
		Endif
				
		Self.IsClosed = True
	
		Return
	End
	
	Method ReceiveMessage:Void()
		UpdateSocket()
		
		Return
	End
	
	Method HL_SetMsgAvail:Void(Value:Bool=False)
		#If RN_HLINTERNALS
			Self.HL_MessageAvail = Value
		#End
		
		Return
	End
	
	Method SetAddress:Void(IP:String, Port:Int)
		Self.IP IP
		Self.EPort = Port
		
		Return
	End
	
	Method UpdateSocket:Void()	
		If (Self.Socket <> Null) Then
			#If QSOCK_ENABLED
				Self.Socket.Update()
			#Endif
			
			#If ANET_ENABLED
				Self.HL_MessageAvail = Socket.ReceiveUDP()
			#Elseif Not RN_COMMUNITY_SOCKETS
				' Nothing so far. This is already done via the async code.
			#End
			
			#If RN_HLINTERNALS
				#If ANET_ENABLED
					If (Self.HL_MessageAvail) Then
				#Elseif Not RN_COMMUNITY_SOCKETS
					'UpdateAsyncEvents()
					
					If (Self.BRL_MessageFound And Self.BRL_InData <> Null) Then
				#End
						'If (Self.InData <> Null) Then Self.InData.Discard()
						'If (Self.InStream <> Null) Then Self.InStream.Close()
						
						'Self.InData = New DataBuffer(Self.Socket._length)
						
						If (Self.InData = Null) Then
							Self.InData = New DataBuffer(Self.BufferSize)
						Endif
						
						If (Self.InStream <> Null) Then
							Self.InStream.Seek(0)
						Elseif (Self.InData <> Null) Then
							Self.InStream = New DataStream(Self.InData)
						Endif
						
						#If ANET_ENABLED
							If (Self.Socket <> Null And Self.Socket._data) Then
								Self.InData.PokeBytes(0, Self.Socket._data, 0, Self.Socket._data.Length())
							Endif
						#End
					Endif
				
				#If Not RN_COMMUNITY_SOCKETS
					Self.HL_MessageAvail = False
					
					If (Self.BRL_MessageFound And Self.BRL_InData <> Null) Then
						' Move this to the standard non-async area, and use BRL_InData.
						'Self.InData = Data
						
						'If (Self.InData <> Null) Then Self.InData.Discard()
						'Self.InData = New DataBuffer(Self.BRL_InData.Length())
						
						'If (Self.InStream <> Null) Then Self.InStream.Close()
						'Self.InStream = New DataStream(Self.InData)
						If (Self.InData <> Null) Then
							Self.BRL_InData.CopyBytes(0, Self.InData, 0, Min(Self.BRL_InData.Length(), Self.InData.Length()))
							
							'Self.BRL_InData.CopyBytes(Offset, Data, 0, Count)
						Endif
						
						If (Self.InStream <> Null) Then
							Self.InStream.Seek(0)
							'Self.InStream.Close()
						Endif
						
						'Self.InStream = New DataStream(Self.InData)
						
						Self.HL_MessageAvail = True
						Self.BRL_MessageFound = False
						
						Self.RelaunchReceiver = True
					Endif
					
					If (Self.RelaunchReceiver) Then
						'Print("|Relaunching packet Receiver|")
						
						'#Rem
						' Self.BufferSize
						
						If (Self.IsServer) Then
							Self.Socket.ReceiveFromAsync(Self.BRL_InData, 0, Self.BRL_InData.Length(), Self.ExternalAddress, Self)
						Else
							Self.Socket.ReceiveAsync(Self.BRL_InData, 0, Self.BRL_InData.Length(), Self)
						Endif
						'#End
						
						Self.RelaunchReceiver = False
					Endif
				#End
			#End
		Endif
	
		Return
	End
	
	Method Send:Void(Msg:String, Type:Int, Reliable:Bool, Regenerate:Bool, DestMode:Int, DestIP:Int, DestPort:Int)
		Send(Msg, Type, Reliable, Regenerate, DestMode, Convert_IntToString_IP(DestIP), DestPort)
		
		Return
	End
	
	Method Send:Void(Msg:String, Type:Int, Reliable:Bool=True, Regenerate:Bool=True, DestMode:Int=RN_DEST_HOST, DestIP:String="", DestPort:Int=0)
		Self.MessageHandler.SendMsg(Self.MessageHandler.MessagePool.Allocate().Init(MessageHandler, Msg, Type), Reliable, Regenerate, DestMode, DestIP, DestPort)
	
		Return
	End
	
	Method Send:Void(Msg:Int[], Type:Int, Reliable:Bool, Regenerate:Bool, DestMode:Int, DestIP:Int, DestPort:Int)
		Send(Msg, Type, Reliable, Regenerate, DestMode, Convert_IntToString_IP(DestIP), DestPort)
		
		Return
	End
	
	Method Send:Void(Msg:Int[], Type:Int, Reliable:Bool=True, Regenerate:Bool=True, DestMode:Int=RN_DEST_HOST, DestIP:String="", DestPort:Int=0)
		Self.MessageHandler.SendMsg(Self.MessageHandler.MessagePool.Allocate().Init(MessageHandler, Msg, Type), Reliable, Regenerate, DestMode, DestIP, DestPort, True)
	
		Return
	End
	
	Method Send:Void(Msg:Object, Type:Int, Reliable:Bool, Regenerate:Bool, DestMode:Int, DestIP:Int, DestPort:Int)
		Send(Msg, Type, Reliable, Regenerate, DestMode, Convert_IntToString_IP(DestIP), DestPort)
		
		Return
	End
	
	Method Send:Void(Msg:Object, Type:Int, Reliable:Bool=True, Regenerate:Bool=True, DestMode:Int=RN_DEST_HOST, DestIP:String="", DestPort:Int=0)
		If (RN_Message(Msg)) Then
			Self.MessageHandler.SendMsg(RN_Message(Msg), Reliable, Regenerate, DestMode, DestIP, DestPort)
		Elseif (DataBuffer(Msg)) Then
			Self.MessageHandler.SendMsg(Self.MessageHandler.MessagePool.Allocate().Init(MessageHandler, DataBuffer(Msg), Type), Reliable, Regenerate, DestMode, DestIP, DestPort, True)
		Elseif (RN_Packet(Msg)) Then
			Self.MessageHandler.SendMsg(Self.MessageHandler.MessagePool.Allocate().Init(MessageHandler, RN_Packet(Msg), Type, False), Reliable, Regenerate, DestMode, DestIP, DestPort, True)
		Else
			' Nothing so far.
		Endif
	
		Return
	End
	
	Method Send:Void(Msg:Object, Type:Int, Reliable:Bool, Regenerate:Bool, DestMode:Int, Connection:RN_Connection)
		Send(Msg, Type, Reliable, Regenerate, DestMode, Connection.IP, Connection.Port)
	
		Return
	End
	
	Method Send:Int(R:RN_InternalMessage)
		Return MessageHandler.SendMsg(R)
	End
	
	Method MsgAvail:Bool()
		Local SecurityResponse:Bool = True
		Local Response:Bool = False
		
		#If QSOCK_ENABLED
			If (Self.Socket <> Null) Then
				Response = Socket.MsgAvail()
			Endif
		#Elseif RN_HLINTERNALS
			Response = Self.HL_MessageAvail
		#Else
			' Nothing so far.
		#End
	
		If (Response And Self.IsServer) Then
			If (MessageHandler <> Null And MessageHandler.Security) Then
				Local ClientConnection:RN_Connection = Null
				
				ClientConnection = MessageHandler.FindConnection(MessageIP(), MessagePort())
								
				Local Initializing:Bool = ReadBool()
			
				If (ClientConnection = Null) Then
					SecurityResponse = False
				Endif
				
				If (Initializing) Then					
					' Read the (usually encrypted) password.
					Local IncomingPassword:String = ReadString()
					
					' Compare the incoming password against our own password.
					If (IncomingPassword = ConnectionPassword Or ConnectionPassword = "") Then					
						SecurityResponse = True
					Else
						' Nothing so far.
					Endif
				Endif
			Elseif (MessageHandler <> Null And Not MessageHandler.Security) Then
				' Nothing so far.
			Endif
		Endif
		
		If (Not SecurityResponse) Then		
			Return False
		Endif
		
		Return Response
	End

	Method SendMsg:Int(IP:Int, Port:Int=0)
		If (Self.IsServer) Then
			If (IP = 0) Then IP = Convert_StringToInt_IP(MessageIP())
			If (Port = 0) Then Port = MessagePort()
		Else
			If (IP = 0) Then IP = Convert_StringToInt_IP(Self.IP)
			If (Port = 0) Then Port = Self.EPort
		Endif
		
		#If QSOCK_ENABLED
			Return Self.Socket.SendMsg(IP, Port)
		#Else'If ANET_ENABLED OR Not RN_COMMUNITY_SOCKETS
			Return Self.SendMsg(Convert_IntToString_IP(IP), Port)
		#End
		
		Return 0
	End
	
	Method SendMsg:Int(IP:String="", Port:Int=0)
		If (Self.IsServer) Then
			If (IP = "") Then IP = MessageIP()
			If (Port = 0) Then Port = MessagePort()
		Else
			If (IP = "") Then IP = Self.IP
			If (Port = 0) Then Port = Self.EPort
		Endif
	
		#If QSOCK_ENABLED
			' Check for the type of IP address (IPV6 or IPV4 (Converted)):
			If (IP.Find(":") <> -1) Then ' STRING_INVALID_LOCATION
				' I still need to add IPV6/String IPs to QuickSock.
				'Return Self.Socket.SendMsg(IP, Port)
				
				' This is temporary, I'll add proper support at some point.
				Return Self.SendMsg(Convert_StringToInt_IP(IP), Port)
			Else
				' In the event that it's an IPV4 address,
				' convert it back to an integer, then call the other method.
				Return Self.SendMsg(Convert_StringToInt_IP(IP), Port)
			Endif
			
		#Elseif RN_HLINTERNALS
			' Some temporary variables:
			'Local TempLength:Int = OutStream.Length()
			Local OutStream_Position:= OutStream.Position()
			Local OutData_Length:= OutData.Length()
			
			#If ANET_ENABLED
				Local TempIP:String = Self.Socket._ip
				Local TempPort:Int = Self.Socket._port
				
				OutData_TempBuffer = RequestIntermediateCache(OutData_Length, False)
			
				' Set the socket's information to the data specified:
				Self.Socket._ip = IP
				Self.Socket._port = Port
				
				OutData.PeekBytes(0, OutData_TempBuffer, 0, OutData_Length)
				
				' Now that we have the information set up, we need to send our message.
				Self.Socket.WriteBytes(OutData_TempBuffer, 0, OutStream_Position)
				
				' Now we can reset the socket's internal variables:
				Self.Socket._ip = TempIP
				Self.Socket._port = TempPort
			#Elseif Not RN_COMMUNITY_SOCKETS
				If (Self.OutData <> Null And Self.BRL_OutData <> Null) Then
					Self.OutData.CopyBytes(0, Self.BRL_OutData, 0, Min(Self.BRL_OutData.Length(), Self.OutData.Length()))
					
					'Self.BRL_InData.CopyBytes(Offset, Data, 0, Count)
				Endif
				
				If (Not IsServer)
					Self.Socket.SendAsync(Self.BRL_OutData, 0, OutStream_Position, Self)
				Else
					Self.ExternalAddress = GetCachedSocketAddress(IP, Port)
					
					Self.Socket.SendToAsync(Self.BRL_OutData, 0, OutStream_Position, Self.ExternalAddress, Self)
				Endif
			#End
			
			' Recreate the data-buffer, and its data-stream:
			#Rem
				If (Self.OutData <> Null) Then
					Self.OutData.Discard()
				Endif
			#End
			
			If (Self.OutStream <> Null) Then
				Self.OutStream.Seek(0)
				'Self.OutStream.Close()
			Endif
			
			'Self.OutData = New DataBuffer(Self.BufferSize)
			'Self.OutStream = New DataStream(Self.OutData)
			
			' Return the length of the message.
			Return OutStream_Position
		#End
		
		' In the event the target wasn't supported, return zero.
		Return 0
	End
		
	' This is method is 100% platform dependent,
	' it may not function the same way on all platforms:
	#If QSOCK_ENABLED
		Method GetSocket:QSocket()
	#ElseIf ANET_ENABLED
		Method GetSocket:Socket()
	#Elseif Not RN_COMMUNITY_SOCKETS
		Method GetSocket:Socket()
	#Else
		' If a target isn't supported,
		' this method will generally return 'Null':
		Method GetSocket:Object()
	#End
			Return Socket
		End
	
	Method Remove_MessageHandler:Bool()
		MessageHandler = Null
		
		' Return the default response.
		Return True
	End
	
	Method Add_MessageHandler:Bool(MH:RN_MessageHandler)
		MessageHandler = MH
		
		Return True
	End
	
	' Writing methods:
	Method WriteString:Bool(Str:String, Length:Int=0, WriteLength:Bool=True, SizeOfChars:Int=1)
		'If (Str = "") Then Return False ' - Do not uncomment this.
		
		If (Length = 0) Then
			Length = Str.Length()
		Endif
		
		Length = Min(Str.Length(), Length)
		
		If (WriteLength) Then
			WriteInt(Length)
		Endif
		
		Return Write(Str.ToChars(), Length, SizeOfChars)
	End
	
	Method WriteByte:Bool(Data:Int)
		#If QSOCK_ENABLED
			Return Self.Socket.WriteByte(Data)
		#Elseif RN_HLINTERNALS
			If (Self.OutStream <> Null And Not Self.OutStream.Eof()) Then
				Self.OutStream.WriteByte(Data)
			Endif
		#Else
			Return False
		#End
		
		' Return the default response.
		Return True
	End
	
	Method WriteByte:Bool(Data:Bool)
		If (Data) Then
			Return WriteByte(1)
		Endif
		
		Return WriteByte(0)
	End
	
	Method WriteBool:Bool(Data:Bool)
		Return WriteByte(Data)
	End
	
	Method WriteBytes:Bool(Data:Int[], Length:Int=0)	
		Return Write(Data, Length, 1)
	End
	
	Method WriteShorts:Bool(Data:Int[], Length:Int=0)
		Return Write(Data, Length, 2)
	End
	
	Method WriteInts:Bool(Data:Int[], Length:Int=0)
		Return Write(Data, Length, 4)
	End
	
	Method WriteLongs:Bool(Data:Int[], Length:Int=0)
		Return Write(Data, Length, 8)
	End
	
	Method Write:Bool(Data:Int[], Length:Int=0, Size:Int=1)
		If (Length <= 0) Then
			Length = Data.Length()
		Else
			Length = Min(Length, Data.Length())
		Endif
	
		#If QSOCK_ENABLED
			Return Self.Socket.Write(Data, Length, Size)
		#Elseif RN_HLINTERNALS
			If (Self.OutStream <> Null And Not Self.OutStream.Eof()) Then
				If (Size > 1) Then
					If (Size > 4) Then
						Size = 8
					Elseif (Size < 4) Then
						Size = 2
					Endif
				Else
					Size = 1
				Endif
				
				'Local TempBuffer:DataBuffer = New DataBuffer(Length*Size)
				
				If (Size > 1) Then
					If (Size < 4) Then
						' INT16:
						'TempBuffer.PokeShorts(0, Data, 0, Length)
						
						For Local I:Int = 0 Until Length
							WriteShort(Data[I])
						Next
						
					Elseif (Size > 4) Then
						' "INT64":
						For Local I:Int = 0 Until Length
							WriteInt(0)
							WriteInt(Data[I])
							
							#Rem
							TempBuffer.PokeInt(I*(Size/2), 0)
							TempBuffer.PokeInt(I*Size, Data[I])
							#End
						Next
					Else
						' INT32:
						'TempBuffer.PokeInts(0, Data, 0, Length)
						
						For Local I:Int = 0 Until Length
							WriteInt(Data[I])
						Next
					Endif
				Else
					' INT8:
					'TempBuffer.PokeBytes(0, Data, 0, Length)
					
					For Local I:Int = 0 Until Length
						WriteByte(Data[I])
					Next
				Endif
				
				#Rem
				If (Self.OutStream.Write(TempBuffer, 0, Length)) Then
					Return True
				Endif
				#End
				
				Return True
			Endif
		#End
	
		Return False
	End

	Method WriteShort:Bool(Data:Int)
		#If QSOCK_ENABLED
			Return Self.Socket.WriteShort(Data)
		#Elseif RN_HLINTERNALS
			If (Self.OutStream <> Null And Not Self.OutStream.Eof()) Then
				If (FixByteOrder) Then
					Data = HToNS(Data)
				Endif
				
				Self.OutStream.WriteShort(Data)
			Endif
			
			Return True
		#Else
			Return False
		#End
	End
	
	Method WriteInt:Bool(Data:Int)	
		#If QSOCK_ENABLED
			Return Self.Socket.WriteInt(Data)
		#Elseif RN_HLINTERNALS
			If (Self.OutStream <> Null And Not Self.OutStream.Eof()) Then
				If (FixByteOrder) Then
					Data = HToNL(Data)
				Endif
				
				Self.OutStream.WriteInt(Data)
			Endif
			
			Return True
		#Else
			Return False
		#End
	End
	
	Method WriteLong:Bool(Data:Int)
		#If QSOCK_ENABLED
			Return Self.Socket.WriteLong(Data)
		#Elseif RN_HLINTERNALS
			If (Self.OutStream <> Null And Not Self.OutStream.Eof()) Then
				WriteInt(0)
				WriteInt(Data)
			Endif
			
			Return True
		#End
	
		Return False
	End
	
	Method WriteFloat:Bool(Data:Float)
		#If QSOCK_ENABLED
			Return Self.Socket.WriteFloat(Data)
		#Elseif RN_HLINTERNALS
			If (Self.OutStream <> Null And Not Self.OutStream.Eof()) Then
				If (FixByteOrder) Then
					Self.OutStream.WriteInt(HToNF(Data))
				Else
					Self.OutStream.WriteFloat(Data)
				Endif
			Endif
			
			Return True
		#End
		
		Return False
	End
	
	' Reading methods:
	Method ReadString:String(Length:Int=0, GetLengthInt:Bool=True, SizeOfChars:Int=1)
		If (GetLengthInt And Length <= 0) Then
			Length = ReadInt()
		Endif
		
		If (Length > 0) Then
			Return String.FromChars(Read(Length, SizeOfChars))
		Else
			'Return String.FromChars(Read(-1, SizeOfChars))
			
			Return ""
		Endif
	End
	
	Method ReadByte:Int()
		#If QSOCK_ENABLED
			If (Self.Socket <> Null) Then
				Return Self.Socket.ReadByte()
			Endif
		#Elseif RN_HLINTERNALS
			If (Self.InStream <> Null And Not Self.InStream.Eof()) Then
				Return Self.InStream.ReadByte()
			Endif
		#End
		
		Return -1
	End
	
	Method ReadBool:Bool()
		If (ReadByte() > 0) Then
			Return True
		Endif
		
		Return False
	End
	
	Method Read:Int[](Count:Int=-1, SizeOfChars:Int=1, UseIntermediateCache:Bool=True)
		If (SizeOfChars > 1) Then
			If (SizeOfChars > 4) Then
				SizeOfChars = 8
			Elseif (SizeOfChars < 4) Then
				SizeOfChars = 2
			Endif
		Else
			SizeOfChars = 1
		Endif
		
		Local Output:Int[]
		
		#If RN_HLINTERNALS
			If (Count = -1) Then
				Count = (Self.BufferSize - Self.InStream.Position)
			Endif
		#End
		
		#If Not RN_HLINTERNALS
			If (Count <> -1) Then
		#End
				' Check the state of the cache usage-flag:
				If (Not UseIntermediateCache) Then
					' Generate a new array, don't bother with the cache.
					Output = New Int[Count]
				Else
					' The output has been cleared for cache usage.
					Output = RequestIntermediateCache(Count)
				Endif
		#If Not RN_HLINTERNALS
			Endif
		#End
		
		' The second condition of this 'If' statement is a fallback for non-HL internals.
		If (Count > 0 Or Count = -1) Then
			#If RN_HLINTERNALS
				If (Self.InStream <> Null And Self.InData <> Null) Then
					If (Self.InStream <> Null And Not Self.InStream.Eof()) Then
						'Self.InStream.Read(D, 0, D.Length())
						
						If (SizeOfChars > 1) Then
							If (SizeOfChars > 4) Then
								For Local I:Int = 0 Until Count ' Output.Length()
									Output[I] = ReadLong()
									
									#Rem
									D.PeekInt(I*(SizeOfChars/2))
									Output[I] = D.PeekInt(I*SizeOfChars)
									#End
								Next
							Elseif (SizeOfChars < 4) Then
								'D.PeekShorts(0, Output, 0, Output.Length()) ' Count
								
								For Local I:Int = 0 Until Count ' Output.Length()
									Output[I] = ReadShort()
								Next
							Else								
								'D.PeekInts(0, Output, 0, Output.Length()) ' Count
								
								For Local I:Int = 0 Until Count ' Output.Length()
									Output[I] = ReadInt()
								Next
							Endif
						Else
							'D.PeekBytes(0, Output, 0, D.Length()) ' Count
							
							For Local I:Int = 0 Until Count ' Output.Length()
								Output[I] = ReadByte()
							Next
						Endif
						
						' Discard the data-buffer.
						'D.Discard()
						
						' Set the local 'D' variable to null.
						'D = Null
					
						Return Output
					Else
						' Discard the data-buffer.
						'D.Discard()
						
						' Set the local 'D' variable to null.
						'D = Null
					Endif
				Else
					'Error("Unexpected error while reading bytes")
				Endif
			#Elseif QSOCK_ENABLED
				If (Self.Socket <> Null) Then
					Return Self.Socket.Read(Count, SizeOfChars, Output, 0)
				Endif
			#Endif
		Endif
		
		Return []
	End
	
	Method ReadBytes:Int[](Count:Int=-1, UseIntermediateCache:Bool=True)
		Return Read(Count, 1, UseIntermediateCache)
	End
	
	Method ReadShorts:Int[](Count:Int=-1, UseIntermediateCache:Bool=True)
		Return Read(Count, 2, UseIntermediateCache)
	End
	
	Method ReadInts:Int[](Count:Int=-1, UseIntermediateCache:Bool=True)
		Return Read(Count, 4, UseIntermediateCache)
	End
	
	Method ReadLongs:Int[](Count:Int=-1, UseIntermediateCache:Bool=True)
		Return Read(Count, 8, UseIntermediateCache)
	End
	
	Method ReadShort:Int()
		#If QSOCK_ENABLED
			If (Self.Socket <> Null) Then
				Return Self.Socket.ReadShort()
			Endif
		#Elseif RN_HLINTERNALS
			If (Self.InStream <> Null And Not Self.InStream.Eof()) Then
				Local Data:= Self.InStream.ReadShort()
				
				If (FixByteOrder) Then
					Data = NToHS(Data)
				Endif
				
				Return Data
			Endif
		#End
		
		Return -1
	End
	
	Method ReadInt:Int()
		#If QSOCK_ENABLED
			If (Self.Socket <> Null) Then
				Return Self.Socket.ReadInt()
			Endif
		#Elseif RN_HLINTERNALS
			If (Self.InStream <> Null And Not Self.InStream.Eof()) Then
				Local Data:= Self.InStream.ReadInt()
				
				If (FixByteOrder) Then
					Data = NToHL(Data)
				Endif
				
				Return Data
			Endif
		#End
		
		Return -1
	End
	
	Method ReadLong:Int()
		#If QSOCK_ENABLED
			If (Self.Socket <> Null) Then
				Return Int(Self.Socket.ReadLong())
			Endif
		#Elseif RN_HLINTERNALS
			If (Self.InStream <> Null And Not Self.InStream.Eof()) Then
				Return Int(ReadInt() + ReadInt())
			Endif
		#End
		
		Return -1
	End
	
	Method ReadFloat:Float()
		' Local variable(s):
		Local Value:Float = 0.0
		
		#If QSOCK_ENABLED
			If (Self.Socket <> Null) Then
				Value = Self.Socket.ReadFloat()
			Endif
		#Elseif RN_HLINTERNALS
			If (Self.InStream <> Null And Not Self.InStream.Eof()) Then				
				If (FixByteOrder) Then
					Value = NToHF(Self.InStream.ReadInt())
				Else
					Value = Self.InStream.ReadFloat()
				Endif
			Endif
		#End
		
		Return Value
	End
		
	' Encryption related:
	Method Encrypt:String(Data:String)
		If (Self.Encryption <> ENCRYPT_DISABLED) Then
			Return String.FromChars(encryption.EncryptArray(Data.ToChars(), Self.Encryption))
		Endif
		
		Return Data
	End
	
	Method Encrypt:Int[](Data:Int[])
		If (Self.Encryption <> ENCRYPT_DISABLED) Then
			Return encryption.EncryptArray(Data, Self.Encryption)
		Endif
		
		Return Data
	End
	
	Method Encrypt:DataBuffer(Data:DataBuffer)
		If (Self.Encryption <> ENCRYPT_DISABLED) Then
			Return encryption.EncryptBuffer(Data, Self.Encryption)
		Else
			' Disabled for now:
			#Rem
			Local Buffer:DataBuffer = New DataBuffer(Data.Length())
			Data.CopyBytes(0, Buffer, 0, Buffer.Length())
			
			Return Buffer
			#End
			
			Return Data
		Endif
		
		Return Null
	End
	
	#Rem
	Method EncryptByRef:Void(Data:Int[])
		encryption.EncryptArrayByRef(Data, Self.Encryption)
	
		Return
	End
	#End
	
	Method GetPassword:String()
		Return Self.ConnectionPassword
	End
	
	' General purpose methods:
	Method GetPacketDescriptor:RN_Packet()
		Return Self.MessageHandler.GetPacketDescriptor()
	End
	
	Method FreePacketDescriptor:Void(R:RN_Packet)
		Self.MessageHandler.FreePacketDescriptor(R)
		
		Return
	End
	
	' Cache-related commands:
	
	#Rem
		SEE THE CACHE NOTES SECTION WITHIN THE CONSTRUCTOR SECTION FOR DETAILS.
	#End
	
	' The first argument of this command is used for differentiation, and explicit management.
	' In other words, allocate an array (Without a size) in your current scope,
	' then send it in like so: Cache = GenerateIntermediateCache(Cache, InsertSizeHere)
	Method GenerateIntermediateCache:Int[](Cache:Int[], RequestedCacheSize:Int, ManageSize:Bool=True)
		' Local variable(s):
		Local CurrentCacheSize:= Cache.Length()
		
		' Make sure we aren't wasting our time:
		If (CurrentCacheSize <> RequestedCacheSize) Then
			' Check if we should resize the array or not:
			If (ManageSize And CurrentCacheSize <> 0) Then
				' Resize the current cache, instead of generating a new one.
				' Please note that this does still produce a different array in most cases.
				Return Cache.Resize(RequestedCacheSize)
			Elseif (Not ManageSize Or CurrentCacheSize < RequestedCacheSize) Then
				' Generate a new array to use as a cache.
				Return New Int[RequestedCacheSize]
			Endif
		Endif
		
		' Return the original cache.
		Return Cache
	End
	
	Method ClearIntermediateCache:Int[](Amount:Int=-1)
		If (Amount <> -1) Then
			Local Intermediate_Cache_Length:= Intermediate_Cache.Length()
			
			If (Intermediate_Cache_Length = 0) Then
				Return GenerateIntermediateCache(Max(BufferSize, Amount))
			Endif
			
			If (Amount > Intermediate_Cache_Length) Then
				' Assignment of the standard cache is handled within this function.
				Return GenerateIntermediateCache(Amount) ' (Intermediate_Cache_Length)
			Endif
		Endif
		
		Return ClearIntermediateCache(Intermediate_Cache, Amount)
	End
	
	Method ClearIntermediateCache:Int[](Cache:Int[], Amount:Int=-1)
		If (Amount = -1) Then
			Amount = Cache.Length()
		Endif
		
		For Local I:Int = 0 Until Min(Amount, Cache.Length())
			Cache[I] = 0
		Next
		
		Return Cache
	End
	
	Method RequestIntermediateCache:Int[](SizeToAllocate:Int=-1, ZeroOut:Bool=False) Property
		If (ZeroOut) Then
			' If requested to do so, clear the cache using the amount specified.
			' This overload already handles assignment of the standard cache.
			ClearIntermediateCache(SizeToAllocate)
		Else
			' Assignment of the standard cache is handled within this function.
			GenerateIntermediateCache(SizeToAllocate)
		Endif
		
		' Return the cache to the user who requested it.
		Return Intermediate_Cache
	End
	
	' These commands reduce the internal cache down to the internal buffer size (Or not at all if smaller):
	Method ReduceIntermediateCache:Int[](ZeroOut:Bool=True, AmountToZeroOut:Int=-1)
		' Call the main implementation with the standard cache.
		Intermediate_Cache = ReduceIntermediateCache(Intermediate_Cache, ZeroOut, AmountToZeroOut)
		
		' Return the result.
		Return Intermediate_Cache
	End
	
	Method ReduceIntermediateCache:Int[](Cache:Int[], ZeroOut:Bool=True, AmountToZeroOut:Int=-1)
		Return GenerateIntermediateCache(Cache, Min(BufferSize, Cache.Length()), Not ZeroOut)
	End
	
	#If Not RN_COMMUNITY_SOCKETS
		Method GetCachedSocketAddress:SocketAddress(IP:String, Port:Int)
			Return AddToSocketAddressCache(IP, Port)
		End
		
		Method AddToSocketAddressCache:SocketAddress(IP:String, Port:Int)
			' Local variable(s):
			Local Address:= FindSocketAddressInCache(IP, Port)
			
			If (Address <> Null) Then
				Return Address
			Endif
			
			' Create a new address, then add it to the internal cache.
			Return AddToSocketAddressCache(New SocketAddress(IP, Port), False)
		End
		
		' This command returns 'Null' if the address was already in the cache.
		Method AddToSocketAddressCache:SocketAddress(Address:SocketAddress, CheckAddress:Bool=True)
			If (CheckAddress) Then
				If (FindSocketAddressInCache(Address) <> Null) Then
					Return Null
				Endif
			Endif
			
			' Add the address to the internal cache.
			CachedSocketAddresses.AddLast(Address)
			
			Return Address
		End
		
		Method FindSocketAddressInCache:SocketAddress(Address:SocketAddress)
			' Check for errors:
			If (Address = Null) Then Return Null
			
			' Check if we have this exact object:
			If (CachedSocketAddresses.Find(Address) <> Null) Then
				Return Address
			Endif
			
			' We couldn't find this object exactly, see if a similar object exists.
			Return FindSocketAddressInCache(Address.Host, Address.Port)
		End
		
		Method FindSocketAddressInCache:SocketAddress(IP:String, Port:Int)
			' Search through all of the cached addresses,
			' and find one which corresponds with the input:
			For Local Address:= Eachin CachedSocketAddresses
				If (Address.Host = IP And Address.Port = Port) Then
					' This address checks out, return it.
					Return Address
				Endif
			Next
			
			' Return the default response.
			Return Null
		End
		
		Method ClearSocketAddressCache:Bool()
			' Clear the socket-address cache.
			CachedSocketAddresses.Clear()
			
			' Return the default response.
			Return True
		End
	#End
		
	' Methods (Private):
	Private

	' Used for all of BRL.Socket's async stuff:	
	#If Not RN_COMMUNITY_SOCKETS
		Method OnReceiveComplete:Void(Data:DataBuffer, Offset:Int, Count:Int, Source:Socket)
			'If (Source <> Self.Socket) Then Return
			
			Self.BRL_MessageFound = True
			
			Return
		End
		
		Method OnConnectComplete:Void(Connected:Bool, Source:Socket)
			'If (Source <> Self.Socket) Then Return
			
			If (Not Self.IsServer) Then
				Self.IsClosed = Not Connected
			Endif
			
			Return
		End
		
		Method OnSendComplete:Void(Data:DataBuffer, Offset:Int, Count:Int, Source:Socket)
			'If (Self.Socket <> Source) Then Return
		
			Return
		End
		
		Method OnSendToComplete:Void(Data:DataBuffer, Offset:Int, Count:Int, Address:SocketAddress, Source:Socket)
			'If (Self.Socket <> Source) Then Return
		
			Return
		End
		
		Method OnReceiveFromComplete:Void (Data:DataBuffer, Offset:Int, Count:Int, Address:SocketAddress, Source:Socket)
			'If (Self.Socket <> Source) Then Return
			
			Self.ExternalAddress = Address
			
			OnReceiveComplete(Data, Offset, Count, Source)
					
			Return
		End
	#End
	
	Public
	
	' Properties (Public):
	Method Ping:Int() Property
		' Local variable(s):
		Local Connection:= CurrentConnection
		
		If (Connection <> Null) Then
			Return Connection.Ping
		Endif
		
		' Return the default response.
		Return 0
	End
	
	Method CurrentConnection:RN_Connection() Property
		Return FindConnection(IP, Port)
	End
		
	Method BufferSize:Int() Property
		Return Self._BufferSize
	End
	
	Method MaxMessageSize:Int() Property
		Return BufferSize-HeaderSize
	End
	
	Method Closed:Bool() Property
		Return IsClosed
	End
	
	Method FixByteOrder:Bool() Property
		Return Self._FixByteOrder
	End
	
	Method FixByteOrder:Void(Input:Bool) Property
		Self._FixByteOrder = Input
		
		If (Socket <> Null) Then
			#If QSOCK_ENABLED
				Socket.FixByteOrder = Input
			#End
		Endif
		
		Return
	End
	
	Method Hosting:Bool() Property
		Return IsServer
	End
	
	Method IntMessageIP:Int() Property
		If (Self.Socket <> Null) Then
			#If QSOCK_ENABLED
				Return Socket.MsgIP() ' Change this later.
			#Elseif ANET_ENABLED
				Return Convert_StringToInt_IP(Socket._ip)
			#End
		Endif
	
		Return 0
	End
		
	Method VirtualIntMessageIP:Int() Property
		If (MessageHandler <> Null) Then
			Return Convert_StringToInt_IP(VirtualMessageIP())
		Endif
		
		Return 0
	End
	
	Method VirtualMessageIP:String() Property
		If (Not IsServer)
			If (MessageHandler <> Null) Then
				Return MessageHandler.VirtualIP
			Endif
		Else
			Return MessageIP()
		Endif
		
		Return ""
	End
	
	Method VirtualMessagePort:Int() Property
		If (Not IsServer)
			If (MessageHandler <> Null) Then
				Return MessageHandler.VirtualPort
			Endif
		Else
			Return MessagePort()
		Endif
	
		Return -1
	End
	
	Method MessageIP:String() Property
		If (Self.Socket <> Null) Then
			#If QSOCK_ENABLED
				Return Convert_IntToString_IP(Socket.MsgIP()) ' Change this later.
			#Elseif ANET_ENABLED
				Return Socket._ip
			#Elseif Not RN_COMMUNITY_SOCKETS
				If (IsServer) Then
					Return ExternalAddress.Host()
				Else
					Return Socket.RemoteAddress.Host()
				Endif
			#Else
				' Nothing so far.
			#End
		Endif
	
		Return ""
	End
	
	Method MessagePort:Int() Property
		If (Self.Socket <> Null) Then
			#If QSOCK_ENABLED
				Return Socket.MsgPort()
			#Elseif ANET_ENABLED
				Return Socket._port
			#Elseif Not RN_COMMUNITY_SOCKETS
				If (IsServer) Then
					Return ExternalAddress.Port()
				Else
					Return Socket.RemoteAddress.Port()
				Endif
			#End
		Endif
		
		Return -1
	End
	
	Method MessageAvailable:Bool() Property
		Return (MessageHandler.MsgStack.Length() > 0)
	End
	
	Method Messages:Stack<RN_Message>() Property
		Return MessageStack
	End
	
	Method MessageStack:Stack<RN_Message>() Property
		Return MessageHandler.MsgStack
	End
	
	Method MsgHandler:RN_MessageHandler() Property
		Return MessageHandler
	End
	
	Method IP:String() Property
		#If Not Not RN_COMMUNITY_SOCKETS
			Return _IP
		#Else
			If (ExternalAddress <> Null) Then
				Return ExternalAddress.Host()
			Endif
			
			Return ""
		#End
	End
	
	Method Port:Int() Property
		Return EPort
	End
	
	Method EPort:Int() Property
		#If Not Not RN_COMMUNITY_SOCKETS
			Return _EPort
		#Else
			If (ExternalAddress <> Null) Then
				Return ExternalAddress.Port()
			Endif
			
			Return 0
		#End
	End
	
	Method IPort:Int() Property
		Return _IPort
	End
	
	' Properties (Private):
	Private
	
	Method BufferSize:Void(Input:Int) Property
		Self._BufferSize = Input
		
		Return
	End
	
	Method IP:Void(Address:String) Property
		#If Not Not RN_COMMUNITY_SOCKETS
			Self._IP = Address
		#Else
			Local EAddr:SocketAddress = Null
			EAddr = New SocketAddress(Address, ExternalAddress.Port())
			
			Self.ExternalAddress = EAddr
		#End
		
		Return
	End
	
	Method EPort:Void(Value:Int) Property
		#If Not Not RN_COMMUNITY_SOCKETS
			Self._EPort = Value
		#Else			
			Local EAddr:SocketAddress = Null
			EAddr = New SocketAddress(ExternalAddress.Host(), Value)
			
			Self.ExternalAddress = EAddr
		#End
		
		Return
	End
	
	Method IPort:Void(Value:Int) Property
		Self._IPort = Value
	
		Return
	End
	
	Public
	
	' Fields (Public):
	Public
	
	' Booleans / Flags:
	Field IsClosed:Bool
	Field IsServer:Bool
	
	' Debugging related:
	Field AlertOnError:Bool
	Field DebugPause:Bool
	Field ReportBlanks:Bool
	
	#If RN_HLINTERNALS
		Field InData:DataBuffer
		Field OutData:DataBuffer
		
		#If Not RN_COMMUNITY_SOCKETS
			Field BRL_InData:DataBuffer
			Field BRL_OutData:DataBuffer
		#Endif
		
		Field InStream:DataStream
		Field OutStream:DataStream
	
		' High-level variables:
		Field HL_MessageAvail:Bool
	#Endif
	
	' The maximum number of packets per cycle.
	Field MaxPacketsPerCycle:Int
	
	' An intermediate cache used for things like message transfers. (Mainly for transferring between buffers and arrays)
	Field Intermediate_Cache:Int[]
	
	' Sockets can be accessed by anyone.
	#If QSOCK_ENABLED
		Field Socket:QSocket
	#Elseif ANET_ENABLED
		Field Socket:Socket
	#Elseif Not RN_COMMUNITY_SOCKETS
		Field Socket:Socket
	#Else
		Field Socket:Object
	#End
	
	' Fields (Private):
	Private
	
	Field _BufferSize:Int
	
	Field _IPort:Int
	
	#If Not RN_COMMUNITY_SOCKETS
		' Used for replies when you're a server.
		' If you're not a server, this is the address of the server.
		Field ExternalAddress:SocketAddress
		
		' A list of cached external addresses.
		Field CachedSocketAddresses:List<SocketAddress>
		
		' This variable is used for the checking with the async code.
		Field BRL_MessageFound:Bool
		
		' A simple variable that tells RegalNET to restart the async-receiving process.
		Field RelaunchReceiver:Bool
	#Else
		Field _EPort:Int
		Field _IP:String
	#Endif
	
	Field _FixByteOrder:Bool
	
	Field MessageHandler:RN_MessageHandler
	Field InitializedOnce:Bool
	
	Field Encryption:Int
	Field ConnectionPassword:String
	
	Field DebugSet:Bool
	
	Public
End