#Rem
DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

#Rem
 Description:
	RegalNET is a high-level networking module created by Anthony Diamond (Sonickidnextgen).
	This module is used by its creator, so you can rest assured it will be fixed if it ever brakes.
		
	RegalNET currently supports the following socket libraries/modules:
		* QuickSock: GLFW, standard C++ (Not really tested, but QuickSock normally works with it), PopCap Sexy Framework (Untested), and most other standard C++ targets.
		* ANet: Android (Java), and possibly OUYA (100% Untested)
		* BRL.Socket: See the BRL.Socket Monkey documentation for details.
	
	RegalNET will automatically choose an underlying socket; however, you can guide its choices with RN_COMMUNITY_SOCKETS.
	By setting RN_COMMUNITY_SOCKETS to 'True', it will choose between the current community driven socket libraries/modules.
	In the event RegalNET doesn't find a community library, it will try BRL.Socket.
	
	By default, RN_COMMUNITY_SOCKETS will be set to false, which means RegalNET will not choose a community driven socket module/library;
	this may change in the future.
	
 List of primary features:
	* Support for GLFW, Android (Java), and whatever BRL.Socket supports (All BRL.Socket code is asynchronous).
	* Basic encryption (Currently only RC4 as a test, it can easily be expanded)
	* Automated connection and disconnection handling.
	* Faked peer-to-peer messaging. (Clients can send messages to each-other manually, or through a reply)
	* Internal and external packets. (Used for basic things like pinging, and initializing; nothing worth noting to the average user)
	* Optional reliable messaging. (Still reliable internally)
	* A simple "virtual address" system. (Not exactly 'virtual', but one client can get another client's IP address from their echoed messages)
	* The possibility to write packets yourself (As long as you conform to the internal header standards. This is not recommended, and not documented.)
	* Basic messaging security. (Clients must identify with a server before anything else can happen. All packets from unidentified sources are considered join requests.)
	* Optional encrypted password identification.
	* Both integer and string IP support for most features. (Usually converts to a string at the end of the day)
#End

' Preprocessor related:

' Defaults:
#RN_COMMUNITY_SOCKETS = False

' Backend Imports:
#If RN_COMMUNITY_SOCKETS
	#If TARGET = "glfw" Or TARGET = "stdcpp"
		#QSOCK_ENABLED = True
	#Else
		#QSOCK_ENABLED = False
	#End
	
	#If TARGET = "android" ' Or LANG = "java"
		#ANET_ENABLED = True
	#Else
		#ANET_ENABLED = False
	#End
	
	#If QSOCK_ENABLED Or ANET_ENABLED
		#RN_COMMUNITY_SOCKETS_NOT_FOUND = False
	#Else
		#RN_COMMUNITY_SOCKETS_NOT_FOUND = True
	#End
	
	#If Not RN_COMMUNITY_SOCKETS_NOT_FOUND
		#RN_SUPPORTED = True 'Not RN_COMMUNITY_SOCKETS_NOT_FOUND
	#End
#Else
	#QSOCK_ENABLED = False
	#ANET_ENABLED = False
#End

' Imports:

' External Imports (Public):
#If QSOCK_ENABLED
	Import quicksock
#Elseif ANET_ENABLED
	Import anet
	
	#RN_HLINTERNALS = True
#Elseif Not RN_COMMUNITY_SOCKETS
	Import brl.socket
	
	#RN_HLINTERNALS = True
	#RN_SUPPORTED = True
#Else
	#RN_SUPPORTED = False
#End

' More defaults:
#If RN_IMPLEMENTED
	#RN_IMPLEMENTED = True
#End

#RN_HLINTERNALS = False

'#Rem
#If Not RN_SUPPORTED
	#Error "RegalNET does not support ${TARGET} (Please make sure everything is in order)"
#End
'#End

' RegalNET's source code:

' Private Imports:
Private

' Native Imports:
Import external

' System/Internal Imports:
Import byteorder
Import encryption
Import retrostrings

Public

' Public Imports:

' System/Internal Imports:
'Import socket
Import instance
Import messages
Import connection
Import packet

' Miscellaneous:
Import destinations

'Import util