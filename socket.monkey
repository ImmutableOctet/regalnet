#Rem
	TO BE REFACTORED:
#End

#Rem
Strict

Public

' Imports:
Import regalnet
Import publicdatastream

' Interfaces:
Interface PacketHandler
	' Methods:
	' Nothing so far.
End

' Classes:
#If RN_HLINTERNALS
Class RN_Socket Extends PublicDataStream Final
#Else
Class RN_Socket Extends Stream Final
#End
	' Global variable(s):
	
	' Defaults:
	Global Default_BufferSize:Int = 2048
	
	' Booleans / Flags:
	Global Default_FixByteOrder:Bool = True
	
	' Constructor(s):
	Method New(BufferSize:Int=Default_BufferSize, FixByteOrder:Bool=Default_FixByteOrder, ShouldResize:Bool=Default_ShouldResize, SizeLimit:Int=NOLIMIT)
		Super.New(BufferSize, ShouldResize, "", SizeLimit, FixByteOrder)
	End
	
	' Methods:
	' Nothing so far.
	
	' Properties:
	Method BufferSize:Int() Property
		Return Buffer.Length()
	End
	
	' Fields:
	' Nothing so far.
End
#End