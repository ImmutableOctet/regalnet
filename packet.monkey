#Rem
DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Imports (Public):
Import publicdatastream

' Imports (Private):
Private

' Internal:
Import instance

Public

' Classes:

' Also known as a 'PacketDescriptor'.
Class RN_Packet Extends PublicDataStream
	' Consant variable(s):
	
	' Defaults:
	Const Default_FixByteOrder:Bool = True
	
	' Constructor(s):
	Method New()
		' Nothing so far.
	End
	
	Method New(ShouldResize:Bool, FixByteOrder:Bool=Default_FixByteOrder)
		' Call the super-class's implementation.
		Super.New(ShouldResize, "", 0, FixByteOrder)
	End
	
	Method New(Instance:RN_Instance, ShouldResize:Bool=True, FixByteOrder:Bool=Default_FixByteOrder)
		Super.New(Instance, ShouldResize, FixByteOrder)
	End
	
	Method New(BufferSize:Int, ShouldResize:Bool=True, FixByteOrder:Bool=Default_FixByteOrder)
		' Call the super-class's implementation.
		Super.New(BufferSize, ShouldResize, "", 0, FixByteOrder)
	End
	
	Method New(Buffer:DataBuffer, Offset:Int=0, ShouldResize:Bool=True, FixByteOrder:Bool=Default_FixByteOrder)
		' Call the super-class's implementation.
		Super.New(Buffer, Offset, ShouldResize, "", 0, FixByteOrder)
	End
	
	Method New(Buffer:DataBuffer, Offset:Int, Length:Int, ShouldResize:Bool=True, FixByteOrder:Bool=Default_FixByteOrder)
		' Call the super-class's implementation.
		Super.New(Buffer, Offset, Length, ShouldResize, "", 0, FixByteOrder)
	End
	
	Method Construct:PublicDataStream(O:Object, ShouldResize:Bool=True, FixByteOrder:Bool=Default_FixByteOrder)
		Local Instance:= RN_Instance(O)
		
		If (Instance <> Null) Then
			' Local variable(s):
			Local B:DataBuffer = Null
			
			If (Not Self.KeepBuffer) Then
				B = New DataBuffer(Instance.BufferSize)
			Endif
			
			FixByteOrder = Instance.FixByteOrder
			
			Super.Construct(B, 0, 0, ShouldResize, "", Instance.BufferSize, FixByteOrder)
			
			Self.KeepBuffer = True
			
			Return Self
		Endif
		
		Return Null
	End
	
	Method Construct:PublicDataStream(ShouldResize:Bool=True, Path:String="", SizeLimit:Int=0, FixByteOrder:Bool=Default_FixByteOrder)
		Return Super.Construct(ShouldResize, Path, SizeLimit, FixByteOrder)
	End
	
	Method Construct:PublicDataStream(Data:DataBuffer, Offset:Int, ShouldResize:Bool=False, Path:String="", SizeLimit:Int=0, FixByteOrder:Bool=Default_FixByteOrder)
		Return Super.Construct(Data, Offset, ShouldResize, Path, SizeLimit, FixByteOrder)
	End
	
	Method Construct:PublicDataStream(Data:DataBuffer, Offset:Int=0, Length:Int=0, ShouldResize:Bool=False, Path:String="", SizeLimit:Int=0, FixByteOrder:Bool=Default_FixByteOrder)
		Return Super.Construct(Data, Offset, Length, ShouldResize, Path, SizeLimit, FixByteOrder)
	End
	
	' Destructor(s):
	' Nothing so far.
	
	' Methods:
	Method Reset:Int()
		Return Seek(0)
	End
	
	Method Begin:Bool()
		Reset()
		
		' Return the default response.
		Return True
	End
	
	' Technically this doesn't need to be called currently;
	' however, you probably should, so you don't have any issues in the future.
	Method Finish:Bool()
		' Return the default response.
		Return True
		
		'Return Begin()
	End
	
	' Wrappers:
	Method BeginPacket:Bool()
		Return Begin()
	End
	
	Method EndPacket:Bool()
		Return Finish()
	End
	
	' Fields:
	' Nothing so far.
End