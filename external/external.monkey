#Rem
DISCLAIMER:

Copyright (c) 2013 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Preprocessor defaults:
'#RN_PARDON_MS = False

' Imports:
#If BRL_GAMETARGET_IMPLEMENTED
	Import mojo.app
#Else
	Import time
#End

#Rem

' External code:

' The Windows 8 part is just to make sure we don't have any problems.
#If Not BRL_GAMETARGET_IMPLEMENTED
	#If TARGET <> "win8" And TARGET <> "winrt" And LANG = "cpp"
		Import "native/millisecs.${LANG}"
	#Elseif LANG <> "cpp" And Not BRL_GAMETARGET_IMPLEMENTED
		#If Not RN_PARDON_MS
			#Error "The 'Millisecs' command is required for this module, please make sure you're using a valid game-target, or a standard C++ target."
		#End
	#End
#End

' External wrapper:
Extern

#If Not BRL_GAMETARGET_IMPLEMENTED And LANG = "cpp" And TARGET <> "win8" And TARGET <> "winrt"
	'#If TARGET <> "glfw" And TARGET <> "win8" And TARGET <> "sexy"
	Function Millisecs:Int()="regalnet::millisecs"
	'#End
#End

Public

' Fallback functions:
#If Not BRL_GAMETARGET_IMPLEMENTED
	#If LANG <> "cpp"
		#If RN_PARDON_MS = 1			
			' The "update" code for 'MS' uses the 'RN_MS_FPS' variable.
			Global RN_MS:Int = 0
			
			Function Millisecs:Int()
				Return RN_MS
			End
		#End
	#End
	
	' This code shouldn't need to be used:
	
	' 60 FPS is assumed by default.
	Global RN_MS_FPS:Int = 60
	Global RN_MS_RATE:Int = (1000/RN_MS_FPS)
	
	Function SetUpdateRate:Int(Rate:Int)
		RN_MS_FPS = Rate
		RN_MS_RATE = Int(1000/RN_MS_FPS)
	
		Return RN_MS_FPS
	End
	
	Function UpdateRate:Int()
		Return RN_MS_FPS
	End
#End

#End