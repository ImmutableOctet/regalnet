#Rem
DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Imports (Public):
Import regalnet
Import publicdatastream

Import brl.databuffer
'Import mojo.app

' Import (Private):
Private

Import external

Public

Class RN_InternalMessage
	' Constant variable(s):
	Const ErrorStr:String = "RegalNET - Error(RN_InternalMessage): "
	
	' Defaults:
	Const Default_Destination:Int = RN_DEST_ALL
	
	Const Default_DiscardInfo:Bool = True
	Const Default_Reliable:Bool = False
	Const Default_Regenerate:Bool = True
	
	' Global variable(s):
	' Nothing so far.
	
	' Constructor(s):
	Method New()
		' Nothing so far.
	End
	
	Method New(Parent:RN_MessageHandler, Standard:Bool, Type:Int, Reliable:Bool=Default_Reliable, Regenerate:Bool=Default_Regenerate, DestIP:String="", DestPort:Int=0, Destination:Int=Default_Destination, Info:DataBuffer=Null, ID:Int=0, InfoLength:Int=-1, DiscardInfo:Bool=Default_DiscardInfo, CloneInfo:Bool=False)
		Init(Parent, Standard, Type, Reliable, Regenerate, DestIP, DestPort, Destination, Info, ID, InfoLength, DiscardInfo, CloneInfo)
	End
	
	Method Init:RN_InternalMessage(Parent:RN_MessageHandler, Standard:Bool, Type:Int, Reliable:Bool=Default_Reliable, Regenerate:Bool=Default_Regenerate, DestIP:String="", DestPort:Int=0, Destination:Int=Default_Destination, Info:DataBuffer=Null, ID:Int=0, InfoLength:Int=-1, DiscardInfo:Bool=Default_DiscardInfo, CloneInfo:Bool=False)		
		If (InfoLength < 0 And Info <> Null) Then
			InfoLength = Info.Length()
		Endif
		
		Local RndSeed:Int = 0
		
		Self.Parent = Parent
		Self.Type = Type
		Self.Reliable = Reliable
		
		If (CloneInfo) Then
			Self.Info = CloneDataBuffer(Info)
		Else
			Self.Info = Info
		Endif
		
		Self.DestIP = DestIP
		Self.DestPort = DestPort
		
		If (Destination = RN_DEST_DIRECT) Then
			If (Self.DestIP.Length() > 0 Or Self.DestPort <> 0) Then
				Destination = RN_DEST_CLIENT
			Else
				If (Not Parent.Parent.IsServer) Then
					Destination = RN_DEST_HOST
				Else
					Destination = RN_DEST_REPLY
				Endif
			Endif
		Endif
		
		Self.Destination = Destination
		
		Self.Standard = Standard
		Self.InfoLength = InfoLength
		Self.DiscardInfo = DiscardInfo
		
		If (Parent <> Null And Parent.Parent <> Null And Parent.Parent.IsServer) Then
			If (Destination = RN_DEST_REPLY Or Destination = RN_DEST_CLIENT) Then
				If (Parent.VirtualIP <> "" And Parent.VirtualPort <> 0) Then
					Self.ReplyAddress = Parent.VirtualIP
					Self.ReplyPort = Parent.VirtualPort
				Else
					If (Self.DestIP = "" And Self.DestPort = 0) Then
						Self.DestIP = Parent.Parent.MessageIP()
						Self.DestPort = Parent.Parent.MessagePort()
					Endif
				Endif
			Elseif (Destination = RN_DEST_HOST) Then
				If (Parent.Parent.AlertOnError) Then
					Print(ErrorStr + "the destination RN_DEST_HOST should not be used by servers.")
				Endif
				
				If (Parent.Parent.DebugPause) Then
					DebugStop()
				Endif
			Endif
		Endif
		
		If (ID <> 0) Then
			Self.UID = ID
		Else
			' Local variable(s):
			'Local Connection:= Parent.FindConnection(Self.DestIP, Self.DestPort)
			
			Self.UID = Parent.GenerateUID()
		Endif
		
		If (Self.Reliable) Then
			Self.Regenerate = Regenerate
			Self.LifeTimer = Millisecs()
		
			Self.Confirmed = 0 ' False
		Else
			Send()
			
			Self.Confirmed = 1 ' True
		Endif
		
		Self.MaxTime = Parent.MessageTimeout
		Self.Lifetime = (Self.MaxTime * 8)
		
		Return Self
	End
	
	' Destructor(s):	
	Method Free:RN_InternalMessage()
		If (Info <> Null) Then
			If (DiscardInfo) Then
				Info.Discard()
			Endif
			
			Info = Null
		Endif
		
		If (Self.Parent <> Null) Then
			Self.Parent.UncheckedMessages.RemoveEach(Self)
			Self.Parent.InternalMessages.RemoveEach(Self)
			
			Self.Parent = Null
		Endif
		
		Self.InfoLength = -1
		Self.UID = 0
		Self.Type = 0
		Self.MaxTime = 0
		Self.UpTime = 0
		Self.LastMS = 0
		Self.Lifetime = 0
		Self.LifeTimer = 0
		Self.DestIP = ""
		Self.DestPort = 0
		Self.Destination = 0
		Self.ReplyAddress = ""
		Self.ReplyPort = 0
		
		' Flags:
		Self.Confirmed = 0 ' False
		
		Self.Standard = False
		Self.Reliable = Default_Reliable
		Self.Regenerate = Default_Regenerate
		Self.DiscardInfo = Default_DiscardInfo
		
		Self.SentOnce = False
		
		Return Self
	End
	
	' Methods:
	Method Send:Int(UseCache:Bool=True)
		If (Not Confirmed) Then
			If (Not Parent.Parent.IsServer) Then
			
				' Detect if we're actually sending to the server or not.
				If ((Destination = RN_DEST_REPLY Or Destination = RN_DEST_CLIENT) And (Self.DestIP = Parent.Parent.IP And Self.DestPort = Parent.Parent.EPort Or Destination = RN_DEST_REPLY And Self.DestIP = "" And DestPort = 0)) Then
					Destination = RN_DEST_HOST
				Endif
		
				' Write the message header:
				
				' Check if we have security on; if that's the case, do the following:
				If (Parent.Security) Then
					If (Self.Standard = False And Type = RN_MessageHandler.MSG_INIT) Then
						' Write the password, and a simple initialization boolean:
					
						' This byte tells RegalNET if the message is a join-request.
						Parent.Parent.WriteBool(True)
						
						' The actual password we need to send.
						Parent.Parent.WriteString(Parent.Parent.GetPassword())
					Elseif (Self.Standard = True Or Type <> RN_MessageHandler.MSG_INIT) Then
						' If we aren't sending an initialization message, tell RegalNET.
						' (This byte tells RegalNET if the message is a join-request or not.)
						Parent.Parent.WriteBool(False)
					Endif
				Endif
			Endif
			
			Parent.WriteMsgHeader(Self.Standard, Reliable, Type, Destination, UID)

			' Here's where all of the client-to-client functionality is detected:
			If (Not Parent.Parent.IsServer) Then
				If (Destination = RN_DEST_CLIENT) Then
					Parent.Parent.WriteByte(True)
					
					Parent.Parent.WriteString(Self.DestIP)
					Parent.Parent.WriteShort(Self.DestPort)
				Elseif (Destination = RN_DEST_REPLY) Then
					Parent.Parent.WriteByte(True)
					
					If (Self.DestIP <> "" And Self.DestPort <> 0) Then
						Parent.Parent.WriteString(Self.DestIP)
						Parent.Parent.WriteShort(Self.DestPort)
					Else
						Parent.Parent.WriteString(Self.ReplyAddress)
						Parent.Parent.WriteShort(Self.ReplyPort)
					Endif
				Else
					Parent.Parent.WriteByte(False)
				Endif
			Else
				' Tell the targeted client who the other guy is:
				If (Self.ReplyAddress <> "" And Self.ReplyPort <> 0) Then
					If (Destination = RN_DEST_CLIENT Or Destination = RN_DEST_REPLY) Then
						Parent.Parent.WriteByte(True)
						Parent.Parent.WriteString(Self.ReplyAddress)
						Parent.Parent.WriteShort(Self.ReplyPort)
					Else
						Parent.Parent.WriteByte(False)
					Endif
				Else
					Parent.Parent.WriteByte(False)
				Endif
			Endif
			
			' Actual information:
			If (Info <> Null) Then
				Local B:Int[]
				
				If (InfoLength > 0) Then
					If (Not UseCache) Then
						B = New Int[InfoLength]
					Else
						B = Parent.RequestIntermediateCache(InfoLength) ' (..., True)
					Endif
					
					' Read the bytes out of the internal buffer, and into the temporary data.
					Info.PeekBytes(0, B, 0, InfoLength)
				Endif
				
				Parent.Parent.WriteInt(InfoLength)
				
				If (B.Length() > 0) Then
					' Write the temporary data to the "socket".
					Parent.Parent.WriteBytes(B)
				Endif
			Endif
			
			' Set the 'SentOnce' flag to true.
			Self.SentOnce = True
			
			' Now that we're done writing, send the message:
			If (Parent.Parent.IsServer) Then
				Return Parent.Parent.SendMsg(Self.DestIP, Self.DestPort)
			Else
				Return Parent.Parent.SendMsg("", 0)
			Endif
		Endif
		
		Return 0
	End
	
	Method Update:Void()
		If (Confirmed = 1) Then Return
		
		If (Millisecs()-Self.LifeTimer > Self.Lifetime) Then
			If (Self.Regenerate) Then
				Self.LifeTimer = Millisecs()
				Self.Confirmed = 2
			Else
				Free()
			Endif
			
			Return
		Endif
		
		If (LastMS <> 0) Then
			UpTime += (Millisecs()-LastMS)
		Endif
		
		If (UpTime > MaxTime) Then
			Send()
		
			UpTime = 0
		Endif
		
		LastMS = Millisecs()
	
		Return
	End

	' Fields (Public):	
	Field Parent:RN_MessageHandler
	Field Info:DataBuffer
	Field InfoLength:Int
	
	Field UID:Int		  ' Int
	Field Type:Int		  ' Short
	Field MaxTime:Int	  ' Int or Long
	Field UpTime:Int	  ' Int or Long
	Field LastMS:Int	  ' Int or Long

	Field Confirmed:Int
	Field Lifetime:Int
	Field LifeTimer:Int
		
	Field DestIP:String
	Field DestPort:Int	  ' Short
	Field Destination:Int ' Byte
	
	Field ReplyAddress:String
	Field ReplyPort:Int	' Short
	
	Field Standard:Bool
	Field Reliable:Bool
	Field Regenerate:Bool
	Field DiscardInfo:Bool
	
	' Fields (Private):
	Private
	
	Field SentOnce:Bool
	
	Public
End

' Functions:
Function CloneDataBuffer:DataBuffer(Input:DataBuffer)
	' Check for errors:
	If (Input = Null) Then Return Null
	
	Return ResizeBuffer(Input, Input.Length(), True)
End

#Rem
Class RN_Pong
	' Constant variable(s):
	
	' Defaults:
	Const Default_Lifetime:Int = 2500
	
	' Constructor(s):
	Method New(ID:Int, Lifetime:Int=Default_Lifetime)
		Init(ID, Lifetime)
	End
	
	Method Init:RN_Pong(ID:Int, Lifetime:Int=Default_Lifetime)
		Self.IsWorking = True
		Self.ID = ID
		Self.Lifetime = Lifetime
		Self.Uptime = Millisecs()
		
		Self.IsWorking = Not CheckErrors()
		
		Return Self
	End
	
	' Destructor(s):
	Method Free:RN_Pong()
		If (Self.Parent <> Null) Then
			Self.Parent.RemovePong(Self)
		Endif
		
		Self.ID = 0
		Self.Uptime = 0
		Self.IsWorking = False
				
		Return Self
	End
	
	' Methods:
	Method CheckErrors:Bool()
		' Return the default response.
		Return False
	End
	
	Method ErrorFound:Bool()
		Return Not Self.IsWorking
	End
	
	Method Update:Void()		
		Self.IsWorking = Not CheckErrors()
		
		If (Millisecs()-Uptime > Lifetime) Then
			Uptime = Millisecs()
			
			Free()
		Endif
	
		Return
	End
	
	' Properties (Public):
	' Nothing so far.
	
	' Properties (Private):
	Private
	
	' Nothing so far.
	
	Public

	' Fields (Public):
	Field Parent:RN_Connection
	Field ID:Int
	
	' Fields (Private):
	Private
	
	Field Uptime:Int
	Field Lifetime:Int
	Field IsWorking:Bool
	
	Public
End
#End