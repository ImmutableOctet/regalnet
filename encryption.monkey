#Rem
DISCLAIMER:

Copyright (c) 2013 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Imports:
Import regalnet
Import retrostrings

'Public

' Functions:
Function EncryptArray:Int[](Data:Int[], EMode:Int=RN_Instance.ENCRYPT_DISABLED)
	Select EMode
		Case RN_Instance.ENCRYPT_RC4
			Return RC4(Data, RN_Instance.EncryptionPassword)
	End Select
	
	' Return the default response.
	Return []
End Function

Function EncryptBuffer:DataBuffer(Data:DataBuffer, EMode:Int=RN_Instance.ENCRYPT_DISABLED)
	Select EMode
		Case RN_Instance.ENCRYPT_RC4
			Local D:DataBuffer = Null
			Local Info:Int[]
			Local EInfo:Int[]
			
			Info = New Int[Data.Length()]
			
			Data.PeekBytes(0, Info, 0, Info.Length())
			EInfo = RC4(Info, RN_Instance.EncryptionPassword)
			
			D = New DataBuffer(EInfo.Length())
			D.PokeBytes(0, EInfo, 0, EInfo.Length())
			
			Return D
		Default
			' Nothing so far.
	End Select

	Return Null
End Function

Function DecryptArray:Int[](Data:Int[], EMode:Int=RN_Instance.ENCRYPT_DISABLED)
	Select EMode
		Case RN_Instance.ENCRYPT_RC4
			Return EncryptArray(Data, EMode)
	End Select
	
	' Return the default response.
	Return []
End Function

#Rem
Function DecryptArrayByRef:Void(Data:Int[], EMode:Int=RN_Instance.ENCRYPT_DISABLED)
	Select EMode
		Case RN_Instance.ENCRYPT_RC4
			EncryptArrayByRef(Data, EMode)
		Default
			' Nothing so far.
	End Select	

	Return
End Function
#End

Function DecryptBuffer:DataBuffer(Data:DataBuffer, EMode:Int=RN_Instance.ENCRYPT_DISABLED)
	Select EMode
		Case RN_Instance.ENCRYPT_RC4
			Return EncryptBuffer(Data, EMode)
		Default
			' Nothing so far.
	End Select

	Return Null
End Function

Private

' Not exactly efficient, but here it is:
Function RC4:Int[](inp:Int[], key:String)
	Return RC4(String.FromChars(inp), key).ToChars()
End Function

Function RC4:String(inp:String, key:String)
	Local S:Int[256]
	Local K:Int[256]

	Local i:Int, j:Int, t:Int, temp:Int, y:Int
	Local Output:String

	For i = 0 To 255
		S[i] = i
	Next

	j = 1
	
	For i = 0 To 255
		If (j > key.Length()) Then
 			j = 1
		Endif
		
		K[i] = Asc(Mid(key,j,1))
		j += 1
	Next

	j = 0
	
	For i = 0 To 255
		j = ( j + S[i] + K[i] ) & 255
		
		temp = S[i]
		S[i] = S[j]
		S[j] = temp
	Next

	i = 0
	j = 0
	
	For Local x:Int = 1 To inp.Length()
		i = (i + 1) & 255
		j = (j + S[i]) & 255
		temp = S[i]
		S[i] = S[j]
		S[j] = temp
		t = (S[i] + (S[j] & 255)) & 255
		y = S[t]
		
		Output += Chr(Asc(Mid(inp,x,1)) ~ y)
	Next
	
	Return Output
End Function

Public