#Rem

DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.

#End

Strict

Public

' Imports (Public):
Import regalnet

' Imports (Private):
Private

Import external

Public

' Constants (Private):
Private

Const Second:Int = 1000 ' Eternity.SecondLength

Public

' Classes:
Class RN_Connection Final
	' Constructor(s):
	Method New(Parent:RN_MessageHandler, IP:Int, Port:Int, Lifetime:Int=(30*Second))
		Construct(Parent, RN_Instance.Convert_IntToString_IP(IP), Port, Lifetime)
	End
	
	Method New(Parent:RN_MessageHandler, IP:String, Port:Int, Lifetime:Int=(30*Second))
		Construct(Parent, IP, Port, Lifetime)
	End
	
	Method Construct:RN_Connection(Parent:RN_MessageHandler, IP:String, Port:Int, Lifetime:Int=(30*Second))
		#If Not RN_COMMUNITY_SOCKETS
			Self.Address = New SocketAddress(IP, Port)
		#Else
			Self.IP = IP
			Self.Port = Port
		#End
		
		Self.Lifetime = Lifetime
		Self.Parent = Parent
		Self.PingSent = False
		Self.LifeTimer = Millisecs()
		
		Self.PongTimer = Millisecs()
		Self.PongIDs = New Deque<Int>()
		
		If (Parent <> Null) Then
			Self.Parent.AddConnection(Self)
		Endif
		
		Self.IsWorking = Not CheckErrors()
		
		Return Self
	End
	
	' Destructor(s):
	Method Free:RN_Connection()
		If (Self.Parent <> Null) Then
			Self.Parent.RemoveConnection(Self)
			Self.Parent = Null
		Endif
		
		If (Self.PongIDs <> Null) Then
			Self.PongIDs.Clear()
			Self.PongIDs = Null
		End
		
		Self.LifeTimer = 0
		Self.Lifetime = 0
		Self.PingTime = 0
		
		Self.PongTimer = 0
		
		Self._Ping = 0
		
		#If Not RN_COMMUNITY_SOCKETS
			Self.Address = Null
		#Else
			Self._IP = ""
			Self._Port = 0
		#End
		
		' Flags / Booleans:
		Self.PingSent = False
		Self.IsWorking = False
		
		Return Self
	End
	
	Method Destroy:RN_Connection()
		Return Free()
	End
	
	' Methods (Public):
	Method CheckErrors:Bool()
		' Nothing so far.
		
		' Return the default response.
		Return False
	End
	
	Method PongLogged:Bool(UID:Int)
		' Local variable(s):
		Local Response:Bool = False
		
		For Local ID:= Eachin PongIDs
			If (ID = UID) Then
				Response = True
				
				Exit
			Endif
		Next
		
		Return Response
	End
	
	Method AddPong:Bool(UID:Int)
		PongIDs.PushLast(UID)
		
		' Return the default response.
		Return True
	End
	
	Method RemovePong:Bool()
		If (Not PongIDs.IsEmpty) Then
			PongIDs.PopFirst()
		Endif
		
		' Return the default response.
		Return True
	End
			
	Method Update:Void()
		Self.IsWorking = Not CheckErrors()
		
		If (Not Self.IsWorking) Then Return
		
		' Local variables:
		Local CurrentTime:Int = Millisecs()-Self.LifeTimer
		
		'If ((((Millisecs()-Self.LifeTimer) - (Self.Lifetime / 2)) Mod (Self.Lifetime/10)) >= (Self.Lifetime/10)) Then
		If (CurrentTime > (Self.Lifetime / 2)) Then
			If (CurrentTime > Self.Lifetime) Then				
				ResetPing()
			Endif
			
			If (Not Self.PingSent) Then
				SendPing()
			Endif
		Endif
		
		If (Millisecs()-PongTimer > PongLifetime) Then
			PongTimer = Millisecs()
			
			RemovePong()
		Endif
		
		#Rem
		For Local Pong:= Eachin PongIDs
			Pong.Update()
		Next
		#End
	
		Return
	End
	
	Method ResetPing:Void()
		Self.PingSent = False
		
		' Reset the 'life-timer'.
		Self.LifeTimer = Millisecs()
		Self.PingTime = 0
		
		Return
	End
	
	Method SendPing:Void()
		If (ErrorFound()) Then Return
	
		If (Not Self.PingSent) Then
			Self.PingID = Parent.GenerateUID()
			
			Parent.SendPing(Self.PingID, IP, Port)
			
			Self.PingSent = True
			Self.PingTime = Millisecs()
		Endif
	
		Return
	End
	
	' Call-back methods:
	Method OnPingReceived:Void()
		Ping = Millisecs()-Self.PingTime
		
		' Reset ping calculation.
		ResetPing()
		
		Return
	End
	
	' Methods (Private):
	Private
	
	' Nothing so far.
	
	Public

	' Properties (Public):
	Method PongLifetime:Int() Property
		Return Parent.PongLifetime
	End
	
	Method Ping:Int() Property
		Return Self._Ping
	End
	
	Method ErrorFound:Bool() Property
		Return Not Self.IsWorking
	End
	
	Method IP:String() Property
		#If RN_COMMUNITY_SOCKETS
			Return Self._IP
		#Else
			Return Address.Host()
		#End
	End
	
	Method Port:Int() Property
		#If RN_COMMUNITY_SOCKETS
			Return Self._Port
		#Else
			Return Address.Port()
		#End
	End
	
	Method Parent:RN_MessageHandler() Property
		Return Self._Parent
	End
	
	Method Parent:Void(Handler:RN_MessageHandler) Property
		Self._Parent = Handler
	
		Return
	End
	
	' Properties (Private):
	Private
	
	Method Ping:Void(Input:Int) Property
		Self._Ping = Input
		
		Return
	End
	
	Method IP:Void(Addr:String) Property
		#If RN_COMMUNITY_SOCKETS
			Self._IP = Addr
		#Else
			Local NewAddr:SocketAddress = Null
			NewAddr = New SocketAddress(Addr, Self.Address.Port())
			
			Self.Address = NewAddr
		#End
		
		Return
	End
	
	Method Port:Void(Value:Int) Property
		#If RN_COMMUNITY_SOCKETS
			Self._Port = Value
		#Else
			Local NewAddr:SocketAddress = Null
			NewAddr = New SocketAddress(Self.Address.Host(), Value)
			
			Self.Address = NewAddr
		#End
		
		Return
	End
	
	Public
	
	' Fields (Public):
	Field PongIDs:Deque<Int>
	'Field PongIDs:List<RN_Pong>
	
	Field PingID:Int

	' Fields (Private):
	Private
	
	Field LifeTimer:Int
	Field Lifetime:Int
	Field PingTime:Int
	Field PingSent:Bool
	Field IsWorking:Bool
	
	Field PongTimer:Int
	
	Field _Ping:Int
	
	#If Not RN_COMMUNITY_SOCKETS
		Field Address:SocketAddress
	#Else
		Field _IP:String
		Field _Port:Int
	#End
	
	Field _Parent:RN_MessageHandler
	
	Public
End