#Rem
DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Imports:
' Nothing so far.

' Constant variables:
Const RN_DEST_HOST:Int			= RN_DESTINATIONS.HOST
Const RN_DEST_REPLY:Int			= RN_DESTINATIONS.REPLY
Const RN_DEST_BROADCAST:Int		= RN_DESTINATIONS.BROADCAST
Const RN_DEST_ALL:Int			= RN_DESTINATIONS.ALL
Const RN_DEST_CLIENT:Int		= RN_DESTINATIONS.CLIENT
Const RN_DEST_DIRECT:Int		= RN_DESTINATIONS.DIRECT

' Classes and Interfaces:
Interface RN_DESTINATIONS
	' Constant variables:

	' Destinations (All of these are externally accessed by using RN_{INSERT_DESTINATION_HERE}):
	
	#Rem
		The 'DIRECT' destination is rarely supported,
		and its effect is based on the context.
		Don't use this outside of RegalNET itself.
	#End
	
	Const DIRECT:Int = -1
	
	#Rem
		The 'HOST' destination targets the host only.
		Servers can't use this destination.
	#End
	
	Const HOST:Int = 0
	
	#Rem
		The 'REPLY' destination targets the last person who messaged you.
		If you don't input an address, this will send a message to
		the last person who sent a message to you.
		
		This destination works for both clients, and servers.
	#End
	
	Const REPLY:Int = 1
	
	#Rem
		The 'BROADCAST' destination targets literally everyone.
		The server and all of its clients will receive messages
		that are sent with this destination.
		
		The only person who doesn't get the message is the sender.
		This works for both clients, and servers.
	#End
	
	Const BROADCAST:Int = 2
	
	' The 'ALL' destination is currently the same as broadcast.	
	Const ALL:Int = BROADCAST ' 2
	
	#Rem
		The 'CLIENT' destination allows you to send packets to any client.
		
		Clients which use this destination send a message to the server,
		that message then gets ignored by the message stack,
		and echoed to the client with the IP and port specified.
		
		This works with both clients and servers, but it's more common for servers.
	#End
	
	Const CLIENT:Int = 3
End