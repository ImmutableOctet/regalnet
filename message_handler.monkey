#Rem

DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.

#End

Strict

' COMPLETED:
' * Reliable messages.
' * Unreliable messages.

' TODO:
' ...

Public

' Imports (Public):
Import regalnet
Import brl.databuffer
Import brl.pool

'Import mojo.app
'Import RN_Test

' Imports (Private):
Private

Import external

Public

' Classes:
Class RN_MessageHandler Implements RN_DESTINATIONS Final	
	' Constant variable(s):
	Const MSG_INIT:Int = 0
	Const MSG_PING:Int = 1
	Const MSG_PONG:Int = 2
	
	' Global variable(s):
	Global ErrorStr:String = "RegalNET - Error(RN_MessageHandler): "
	
	' Defaults:
	Global Default_MessagePoolSize:Int = 24
	Global Default_PacketDescriptorPoolSize:Int = 1
	
	' Functions:
	' Nothing so far.
	
	' Constructor(s):
	Method New(Parent:RN_Instance, MessageTimeout:Int=500, PongLifetime:Int=-1, ConnectionTimeout:Int=10000) ' In milliseconds.
		If (PongLifetime = -1) Then
			PongLifetime = ConnectionTimeout
		Endif
		
		Self._Parent = Parent
		Self.MessageTimeout = MessageTimeout
		
		Self.Security = True
	
		Self.MessageStack = New Stack<RN_Message>()
		Self.InternalMessages = New Stack<RN_InternalMessage>()
		Self.UncheckedMessages = New List<RN_InternalMessage>()
		
		Self.UID_Blacklist = New List<Int>()
		
		Self.Connections = New List<RN_Connection>()
		Self.Disconnected = New List<RN_Connection>()
		
		Self.PongLifetime = PongLifetime
		Self.ConnectionLife = ConnectionTimeout
		
		Self.MessagePool = New Pool<RN_Message>(Default_MessagePoolSize)
		Self.InternalMessagePool = New Pool<RN_InternalMessage>(Default_MessagePoolSize)
		Self.PacketDescriptorPool = New Pool<RN_Packet>(Default_PacketDescriptorPoolSize)
	End
	
	' Destructor(s):
	
	' This should only be called by RegalNET:
	Method Destroy:Bool()
		MessageStack.Clear()
		MessageStack = Null
		
		InternalMessages.Clear()
		InternalMessages = Null
		
		UncheckedMessages.Clear()
		UncheckedMessages = Null
				
		UID_Blacklist.Clear()
		UID_Blacklist = Null
		
		ClearConnections()
		
		Return True
	End
	
	Method Free:Bool()
		If (Parent <> Null) Then
			Parent.Remove_MessageHandler()
		Endif
	
		Return Destroy()
	End
	
	' Methods (Public):
	Method GetPacketDescriptor:RN_Packet()
		Return RN_Packet(PacketDescriptorPool.Allocate().Construct(Parent, True))
	End
	
	Method FreePacketDescriptor:Void(R:RN_Packet)
		R.Close()
		
		PacketDescriptorPool.Free(R)
		
		Return
	End
	
	Method GenerateUID:Int()
		LastUID += 1
		
		While (PongLogged(LastUID))
			LastUID += 1
		Wend
		
		For Local Blacklisted:= Eachin UID_Blacklist
			While (LastUID = Blacklisted)
				LastUID += 1
			Wend
		Next
		
		Return LastUID
		
		#Rem
		' Local variable(s):
		Local RndResponse:Bool = False
		Local UID:Int = 0
		Local RndSeed:Int = Seed
		
		While (Not RndResponse)
			' Assign a basic seed for the UID randomization.
			Seed = ((Millisecs() * Max(Float(Seed/128), 1.0)) + 1000/Max(Float(RndSeed * 4), 1.0)) / 10
			
			' Set the UID to a randomly generated number (Signed 31-bit int currently).
			UID = Rnd((-Pow(2,30))+1, (Pow(2,30))-1)
			
			' Reset the randomization seed.
			Seed = RndSeed
			
			' If the UID is zero, continue looping.
			If (UID = 0) Then Continue
			
			' Set the randomization response to true.
			RndResponse = True
			
			' Check if anything else is using this:
			For Local I:= Eachin InternalMessages
				If (I.Reliable And I.UID = UID And Not I.Confirmed) Then
					RndResponse = False
					
					Exit
				Endif
			Next
			
			If (RndResponse) Then			
				For Local U:= Eachin UID_Blacklist
					If (UID = U) Then
						RndResponse = False
						
						Exit
					Endif
				Next
			Endif
		Wend
		
		' Return the final UID.
		Return UID
		#End
	End
		
	Method ToggleSecurity:Bool()
		Security = Not Security
		
		Return Security
	End
	
	Method Flush:Void(MSGSTACK:Bool=True, IMSGS:Bool=True, UNCHECKED_MSGS:Bool=True, UID_BLACKLIST:Bool=False)
		If (MSGSTACK) Then
			If (MessageStack <> Null) Then
				MessageStack.Clear()
			Endif
		Endif
		
		If (IMSGS) Then
			If (InternalMessages <> Null) Then
				InternalMessages.Clear()
			Endif
		Endif
		
		If (UNCHECKED_MSGS) Then
			If (UncheckedMessages <> Null) Then
				UncheckedMessages.Clear()
			Endif
		Endif
		
		If (UID_BLACKLIST) Then
			If (UID_Blacklist <> Null) Then
				UID_Blacklist.Clear()
			Endif
		Endif
	
		Return
	End
	
	Method Update:Void()
		If (Parent = Null) Then Return
		
		' The actual update routines:
		
		' Check for incoming messages,
		' and if there's a message, read it.
		CheckMessages()
		
		' Update internal functionality.
		UpdateInternal()
		
		' Update each of the connections.		
		UpdateConnections()
			
		Return
	End
	
	Method UpdateInternal:Void()
		' Grab an object off of the stack.
		Local I:RN_InternalMessage = Null
		
		For Local M:RN_InternalMessage = Eachin UncheckedMessages
			If (M.Confirmed) Then
				InternalMessages.RemoveEach(M)
				InternalMessagePool.Free(M.Free())
				
				Continue
			Elseif (M.Confirmed = 2) Then
				' If it hasn't been confirmed, but its timer is greater than its lifetime:
				UncheckedMessages.RemoveEach(M)
				InternalMessages.Push(M)
				M.Confirmed = 0 ' False
			Endif
			
			M.Update()
		Next
		
		If (InternalMessages.Length() > 0) Then
			For I = Eachin InternalMessages
				'I = InternalMessages.Pop()
				
				If (InternalMessages.Contains(I)) Then
					InternalMessages.RemoveEach(I)
				Endif
				
				' Check to see if the message we pulled from the stack is null or not.
				If (I <> Null) Then
					SendInternal(I)
				Endif
			Next
		Endif
		
		Return
	End
	
	Method UpdateConnections:Void()
		For Local C:RN_Connection = Eachin Connections
			C.Update()
		Next
	
		Return
	End
	
	Method CheckMessages:Void()
		If (Parent = Null) Then Return
		
		' Read all incoming packets:
		#If QSOCK_GLFW
			For Local Attempt:Int = 1 To Parent.MaxPacketsPerCycle
		#End
		
		Parent.UpdateSocket()
	
		If (MsgAvail()) Then
			ReadIncomingMessage()
		Else
			#If QSOCK_GLFW
				Exit
			#End
		Endif
		
		#If QSOCK_GLFW
			Next
		#End
		
		Return
	End
	
	Method MsgAvail:Bool()
		Return Parent.MsgAvail()
	End
		
	Method ReadMessage:Bool()
		Return ReadIncomingMessage()
	End
	
	Method ReadIncomingMessage:Bool()
		' Local variables:
		Local IsMessage:Bool = True
		Local Type:Int = 0
		Local UID:Int = 0
		
		#If RN_HLINTERNALS
			Parent.HL_SetMsgAvail(False)
		#End
		
		IsMessage = Parent.ReadBool()
		
		If (IsMessage) Then
			ReadStdMessage()
		Else
			Type = Parent.ReadByte() ' Non-standard packet type.
			
			Select Type
				Case MSG_PING
					UID = Parent.ReadInt()
					Self.SendPong(UID, Parent.MessageIP(), Parent.MessagePort())
					
				Case MSG_PONG
					' Local variable(s):
					Local MsgResponse:Bool = False
					
					UID = Parent.ReadInt()
					
					For Local M:= Eachin UncheckedMessages
						If (M.UID = UID) Then							
							If (Not M.Confirmed)
								M.Confirmed = True
								
								MsgResponse = True
							#Rem
								Else
									Exit
							#End
							Endif
						Endif
					Next
					
					If (MsgResponse) Then
						If (Connections <> Null) Then
							For Local C:= Eachin Connections
								If (C.PingID = UID) Then
									C.OnPingReceived()
								Endif
							Next
						Endif
					Endif
					
				Case MSG_INIT
					If (Parent.IsServer) Then						
						UID = Parent.ReadInt()
						
						Self.SendPong(UID, Parent.MessageIP(), Parent.MessagePort())
						
						Local Connection:= FindConnection(Parent.MessageIP(), Parent.MessagePort())
						
						' Create a new connection to the client in question.
						If (Connection = Null) Then
							Connection = New RN_Connection(Self, Parent.MessageIP(), Parent.MessagePort(), Self.ConnectionLife)
						Else							
							' Nothing so far.
						Endif
						
						If (Not PongLogged(UID, Connection)) Then
							Connection.AddPong(UID)
						Endif
					Endif
					
				Default
					' Nothing so far.
			End Select
		Endif
	
		Return True
	End
	
	Method PongLogged:Bool(UID:Int)
		' Local variable(s):
		Local PongResponse:Bool = False
		
		For Local C:= Eachin Connections
			PongResponse = PongLogged(UID, C)
			
			If (PongResponse) Then Exit
		Next
		
		Return PongResponse
	End
	
	Method PongLogged:Bool(UID:Int, Connection:RN_Connection)		
		If (Connection = Null) Then
			Return False
		Endif
		
		Return Connection.PongLogged(UID)
	End
	
	Method WriteMsgHeader:Void(Standard:Bool, Reliable:Bool, Type:Int, Destination:Int, UID:Int=0)
		' Total size of standard header: (8 Or 12) + (Optional-Password: SizeOf(Password) + Password Length) + (Optional-IP: SizeOf(IP) + IP Length) + (Optional-Port: 2 Bytes)
		Parent.WriteByte(Standard) ' Message?
		
		' Generate the UID:
		If (Reliable Or Not Standard) Then
			If (UID = 0) Then
				'Local Connection:= FindConnection(Parent.IP, Parent.Port)
				
				UID = GenerateUID()
			Endif
		Endif
			
		If (Standard) Then
			Parent.WriteByte(Reliable)		' Reliable?
			Parent.WriteByte(Destination)	' Destination.
			Parent.WriteShort(Type)			' The type of message.
			
			If (Reliable) Then
				Parent.WriteInt(UID)		' The message's unique ID.
			Endif
		Else
			Parent.WriteByte(Type) ' The type of non-standard message.
			
			Select Type
				Case MSG_INIT
					Parent.WriteInt(UID)
			
				' This may stay like this, I'm not sure yet:
				Case MSG_PING, MSG_PONG
					Parent.WriteInt(UID)
			End Select
		Endif
		
		Return
	End
	
	Method WriteInitMsg:Bool(_IP:Int, _Port:Int)
		Return WriteInitMsg(RN_Instance.Convert_IntToString_IP(_IP), _Port)
	End
	
	Method WriteInitMsg:Bool(_IP:String, _Port:Int)
		#If RN_SUPPORTED
			Local InitMsg:RN_InternalMessage = InternalMessagePool.Allocate().Init(Self, False, MSG_INIT, True, True, _IP, _Port, RN_DEST_HOST)
			
			SendMsg(InitMsg)

			Return True
		#Else
			Return False
		#Endif
	End
	
	Method SendMsg:Void(R:RN_Message, Reliable:Bool=True, Regenerate:Bool=True, DestMode:Int=RN_DEST_HOST, DestinationIP:String="", DestinationPort:Int=0, InputIsMalleable:Bool=False)
		If (R = Null) Then Return
		
		If (Parent.IsServer And ((DestMode = RN_DEST_ALL Or DestMode = RN_DEST_BROADCAST) And DestinationIP.Length()=0 And DestinationPort = 0)) Then			
			For Local C:= Eachin Connections
				SendMsg(R, Reliable, Regenerate, RN_DEST_CLIENT, C.IP, C.Port, False)
			Next
		Else
			InternalMessages.Push(InternalMessagePool.Allocate().Init(Self, True, R.Type, Reliable, Regenerate, DestinationIP, DestinationPort, DestMode, R.Buffer, 0, R.Length, True, True))
		Endif
		
		If (InputIsMalleable) Then
			MessagePool.Free(R.Free())
		Endif
		
		Return
	End
	
	Method SendMsg:Void(R:RN_Message, Reliable:Bool=True, Regenerate:Bool=True, DestMode:Int=RN_DEST_HOST, DestinationIP:Int=0, DestinationPort:Int=0, InputIsMalleable:Bool=False)
		SendMsg(R, Reliable, Regenerate, DestMode, RN_Instance.Convert_IntToString_IP(DestinationIP), DestinationPort, InputIsMalleable)
		
		Return
	End
	
	Method SendMsg:Int(R:RN_InternalMessage)
		If (Not R.Confirmed) Then
			InternalMessages.Push(R)
			
			If (R.Info <> Null) Then
				Return R.Info.Length()
			Else
				If (R.Standard) Then
					If (Parent.DebugPause And Parent.ReportBlanks) Then
						If (Parent.AlertOnError) Then
							Print(ErrorStr + "Internal message found with no information.")
						Endif
						
						DebugStop()
					Endif
				Endif
			
				Return -1
			Endif
		Else
			Return SendInternal(R)
		Endif
		
		Return 0
	End
	
	Method SendInternal:Int(M:RN_InternalMessage)
		UncheckedMessages.AddLast(M)
		
		Return M.Send()
	End
	
	Method SendPong:Void(UID:Int, IP:Int=0, Port:Int=0)
		SendPong(UID, RN_Instance.Convert_IntToString_IP(IP), Port)
		
		Return
	End
	
	Method SendPong:Void(UID:Int, IP:String="", Port:Int=0)
		SendMsg(InternalMessagePool.Allocate().Init(Self, False, MSG_PONG, False, False, IP, Port, RN_DEST_REPLY, Null, UID))
	
		' Outdated version:
		#Rem
			If (Security) Then
				Parent.WriteBool(False)
			Endif
			
			Parent.WriteByte(False)	 ' Message?
			Parent.WriteByte(MSG_PONG)	 ' Type of non-standard message. (Confirmation)
			Parent.WriteInt(UID) ' The message's unique ID.
			
			Parent.SendMsg(IP, Port)
		#End
		
		Return
	End
	
	Method SendPing:Void(UID:Int, IP:Int=0, Port:Int=0, Destination:Int=RN_DEST_DIRECT)
		SendPing(UID, RN_Instance.Convert_IntToString_IP(IP), Port, Destination)
		
		Return
	End
	
	Method SendPing:Void(UID:Int, IP:String="", Port:Int=0, Destination:Int=RN_DEST_DIRECT)
		If (Destination = RN_DEST_DIRECT) Then
			If (Parent.IsServer) Then
				Destination = RN_DEST_CLIENT
			Else
				Destination = RN_DEST_HOST
			Endif
		Endif
		
		SendMsg(InternalMessagePool.Allocate().Init(Self, False, MSG_PING, True, False, IP, Port, Destination, Null, UID))
		
		Return
	End
	
	Method ConnectionFound:RN_Connection(IP:Int, Port:Int)
		Return FindConnection(IP, Port)
	End
	
	Method ConnectionFound:RN_Connection(IP:String, Port:Int)
		Return FindConnection(IP, Port)
	End
	
	Method FindConnection:RN_Connection(IP:Int, Port:Int)
		Return FindConnection(Parent.Convert_IntToString_IP(IP), Port)
	End
	
	Method FindConnection:Bool(C:RN_Connection)		
		If (FindConnection(C.IP, C.Port)) Then
			Return True
		Endif
		
		' Return the default response.
		Return False
	End
	
	Method FindConnection:RN_Connection(IP:String, Port:Int)
		' Local variable(s):
		Local C:RN_Connection = Null
		Local Response:Bool = False
				
		If (IP.Length() = 0) Then
			IP = Parent.MessageIP()
		Endif
		
		If (Port = 0) Then
			Port = Parent.MessagePort()
		Endif
		
		If (Connections <> Null) Then
			For C = Eachin Connections
				If (((IP = C.IP) And (Port = C.Port))) Then
					Response = True
					
					Exit
				Endif
			Next
		Endif
	
		If (Response) Then
			Return C
		Endif
		
		Return Null
	End
	
	Method FlushConnections:Bool()
		Local Response:Bool = False
	
		If (Connections <> Null) Then
			For Local C:RN_Connection = Eachin Connections
				C.Free()
			Next
		
			Connections.Clear()
			Response = True
		Endif
		
		If (Disconnected <> Null) Then
			For Local C:RN_Connection = Eachin Disconnected
				C.Free()
			Next
		
			Disconnected.Clear()
			Response = True
		Endif
			
		Return Response
	End
	
	Method ClearConnections:Bool()
		Local Response:Bool = False

		Response = FlushConnections()
		
		Connections = Null
		Disconnected = Null
	
		Return Response
	End
	
	Method AddConnection:Bool(C:RN_Connection)
		If (C <> Null) Then
			If (Disconnected <> Null) Then
				If (Disconnected.Contains(C)) Then
					Disconnected.RemoveEach(C)
				Else
					For Local Connection:RN_Connection = Eachin Disconnected
						If (Connection.IP = C.IP And Connection.Port = C.Port) Then
							Disconnected.RemoveEach(Connection)
						Endif
					Next
				Endif
			Endif
		
			If (Connections <> Null And Connections.Contains(C)=False And Not FindConnection(C)) Then
				C.Parent = Self
				Connections.AddLast(C)
			Endif
		Endif
	
		Return True
	End
	
	Method RemoveConnection:Bool(C:RN_Connection)
		Local Response:Bool = False
		
		If (C <> Null) Then
			If (Connections <> Null And Connections.Contains(C)) Then
				Connections.RemoveEach(C)
				
				Response = True
			Endif

			If (Disconnected <> Null And Disconnected.Contains(C)) Then
				Disconnected.RemoveEach(C)
				
				Response = True
			Endif
		Endif
		
		Return Response
	End
	
	Method SetParent:Bool(Parent:RN_Instance)
		Self.Parent.Remove_MessageHandler()
		Self.Parent = Parent
		Self.Parent.Add_MessageHandler(Self)
	
		Return True
	End
	
	' This command simply wraps the 'RN_Instance' version by calling it through the 'Parent' property.
	Method RequestIntermediateCache:Int[](SizeToAllocate:Int=-1, ZeroOut:Bool=False) Property
		Return Parent.RequestIntermediateCache(SizeToAllocate, ZeroOut)
	End
	
	' Methods (Private):
	Private
	
	Method ReadStdMsg:Void()
		ReadStdMessage()
		
		Return
	End
	
	Method ReadStdMessage:Bool()		
		' Local variable(s):
		Local SecurityAlert:Bool = False
		Local PongResponse:Bool = False
		Local IsReliable:Bool = False
		
		Local Destination:Int = 0
		Local Type:Int = 0
		Local UID:Int = 0
		
		Local DestIP:String
		Local DestPort:Int = 0
		
		Local BufferSize:Int = 0
		'Local Buffer:DataBuffer = Null
		
		IsReliable = Parent.ReadBool()
		Destination = Parent.ReadByte()		
		Type = Parent.ReadShort()
		
		If (IsReliable) Then
			UID = Parent.ReadInt()
		Endif
		
		If (IsReliable) Then
			If ((Destination = RN_DEST_ALL Or Destination = RN_DEST_BROADCAST Or Destination = RN_DEST_HOST) And Self.Parent.IsServer Or Not Self.Parent.IsServer) Then ' And Destination <> RN_DEST_HOST
				Local Connection:= FindConnection(Parent.MessageIP(), Parent.MessagePort())
				
				If (Connection <> Null And Not PongLogged(UID, Connection)) Then
					'Connection.AddPong(New RN_Pong(UID, Self.PongLifetime))
					Connection.AddPong(UID)
					
					PongResponse = True
				Endif
			Endif
		Else
			PongResponse = True
		Endif
		
		If (PongResponse) Then
			If (Parent.ReadBool()) Then
				Select Destination
					Case RN_DEST_REPLY, RN_DEST_CLIENT
						DestIP = Parent.ReadString()
						DestPort = Parent.ReadShort()
						
						If (Security) Then
							If (Parent.IsServer) Then
								SecurityAlert = True
								
								If (FindConnection(DestIP, DestPort)) Then
									SecurityAlert = False
								Endif
							Else
								SecurityAlert = False
							Endif
						Endif
						
						If (Not SecurityAlert) Then
							If (Parent.IsServer) Then
								Self.VirtualIP = Parent.MessageIP()
								Self.VirtualPort = Parent.MessagePort()
								
								If (Security) Then
									SecurityAlert = True
								
									If (ConnectionFound(Self.VirtualIP, Self.VirtualPort)) Then
										SecurityAlert = False
									Endif
								Endif
							Else
								Self.VirtualIP = DestIP
								Self.VirtualPort = DestPort
							Endif
						Endif
					'Default
						' Nothing so far.
				End Select
			Else
				If (Not Parent.IsServer) Then
					Self.VirtualIP = Parent.MessageIP()
					Self.VirtualPort = Parent.MessagePort()
				Endif
			Endif
			
			If (Not SecurityAlert) Then
				BufferSize = Parent.ReadInt()
				
				Local IBuffer:= Parent.ReadBytes(BufferSize, True)
		
				If (IsReliable) Then
					Self.SendPong(UID, Parent.MessageIP(), Parent.MessagePort())
				Endif
				
				If (Self.Parent.IsServer) Then
					Select Destination
						Case RN_DEST_REPLY, RN_DEST_CLIENT
							' In the event we need to, bounce the message to another client.					
							Parent.Send(IBuffer, Type, IsReliable, True, RN_DEST_CLIENT, DestIP, DestPort)
							
							' Reset the virtual IP and port (Server):
							Self.VirtualIP = ""
							Self.VirtualPort = 0
						Case RN_DEST_ALL, RN_DEST_BROADCAST							
							For Local C:= Eachin Connections
								If ((C.IP = Self.VirtualIP And C.Port = Self.VirtualPort) Or (C.IP = Parent.MessageIP() And C.Port = Parent.MessagePort())) Then
									' Nothing so far.
								Else
									Self.VirtualIP = Parent.MessageIP()
									Self.VirtualPort = Parent.MessagePort()
									
									Parent.Send(IBuffer, Type, IsReliable, True, RN_DEST_CLIENT, C.IP, C.Port)
								Endif
							Next
							
							' Reset the virtual IP and port (Server):
							Self.VirtualIP = ""
							Self.VirtualPort = 0
						Default 'Case RN_DEST_HOST
							' Nothing so far.
					End Select
				Endif
				
				' Add the new message to the message stack.
				MessageStack.Push(MessagePool.Allocate().Init(Self, IBuffer, Type))
			Endif
		Else
			Parent.HL_SetMsgAvail(False)
		Endif
	
		Return True
	End
	
	Public
	
	' Properties (Public):
	Method MessageAvailable:Bool() Property
		Return Parent.MessageAvailable()
	End
	
	Method MsgStack:Stack<RN_Message>() Property
		Return MessageStack
	End
		
	Method Parent:RN_Instance() Property
		Return Self._Parent
	End
	
	' A quick wrapper for our parent's intermediate-cache.
	' This is here if needed, but you really should just make a formal request.
	Method Intermediate_Cache:Int[]() Property
		Return Parent.Intermediate_Cache
	End
	
	' Properties (Private):
	Private
	
	Method MsgStack:Void(M:Stack<RN_Message>) Property
		MessageStack = M
		
		Return
	End
	
	Method Parent:Void(P:RN_Instance) Property
		Self._Parent = P
		
		Return
	End
	
	Public

	' Fields (Public):
	
	' Pools:
	Field MessagePool:Pool<RN_Message>
	Field InternalMessagePool:Pool<RN_InternalMessage>
	Field PacketDescriptorPool:Pool<RN_Packet>
	
	Field Security:Bool
	
	Field MessageTimeout:Int
	Field PongLifetime:Int
	Field ConnectionLife:Int
	
	Field InternalMessages:Stack<RN_InternalMessage>
	Field UncheckedMessages:List<RN_InternalMessage>
	
	Field VirtualIP:String
	Field VirtualPort:Int
	
	' Server specific fields:
	Field Connections:List<RN_Connection>
	Field Disconnected:List<RN_Connection>
	
	' Etc:
	Field LastUID:Int
	Field UID_Blacklist:List<Int>
	
	' Fields (Private):
	Private	
	
	Field MessageStack:Stack<RN_Message>
	Field _Parent:RN_Instance
	
	'Field Message_PongStack:Stack<InternalMessage_Pong>
	
	Public
End