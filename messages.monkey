#Rem
DISCLAIMER:

Copyright (c) 2014 - Anthony Diamond

Permission is hereby granted, free of charge, To any person obtaining a copy of this software And associated documentation files (the "Software"),
To deal in the Software without restriction, including without limitation the rights To use, copy, modify, merge, publish, distribute, sublicense,
And/Or sell copies of the Software, And To permit persons To whom the Software is furnished To do so, subject To the following conditions:

The above copyright notice And this permission notice shall be included in all copies Or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS Or IMPLIED, INCLUDING BUT Not LIMITED To THE WARRANTIES OF MERCHANTABILITY,
FITNESS For A PARTICULAR PURPOSE And NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS Or COPYRIGHT HOLDERS BE LIABLE For ANY CLAIM,
DAMAGES Or OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT Or OTHERWISE, ARISING FROM,
OUT OF Or IN CONNECTION WITH THE SOFTWARE Or THE USE Or OTHER DEALINGS IN THE SOFTWARE.
#End

Strict

Public

' Imports:
Import message_handler
Import packet
Import internal_messages
Import brl.databuffer

'Public

' Classes:
Class RN_Message Extends RN_Packet
	' Constant variable(s):
	
	' Defaults:
	Const Default_DiscardBuffer:Bool = True
	
	' Constructor(s):
	Method New()
		' Nothing so far.
	End
	
	Method New(Parent:RN_MessageHandler, P:RN_Packet, MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)
		Init(Parent, P, MSGType, DiscardBuffer)
	End
	
	Method New(Parent:RN_MessageHandler, INFO:DataBuffer, MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)
		Init(Parent, INFO, MSGType, DiscardBuffer)
	End
	
	Method New(Parent:RN_MessageHandler, INFO:Int[], MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)
		Init(Parent, INFO, MSGType, DiscardBuffer)
	End
	
	Method New(Parent:RN_MessageHandler, INFO:String, MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)
		Init(Parent, INFO, MSGType, DiscardBuffer)
	End
	
	Method Init:RN_Message(Parent:RN_MessageHandler, P:RN_Packet, MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)		
		Return Init(Parent, P.Buffer, MSGType, P.Position, DiscardBuffer)
	End
	
	Method Init:RN_Message(Parent:RN_MessageHandler, INFO:Int[], MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)
		Self.Buffer = New DataBuffer(INFO.Length())
		Self.Buffer.PokeBytes(0, INFO)

		Return Init(Parent, Self.Buffer, MSGType, Self.Buffer.Length(), DiscardBuffer)
	End
	
	Method Init:RN_Message(Parent:RN_MessageHandler, INFO:String, MSGType:Int, DiscardBuffer:Bool=Default_DiscardBuffer)
		Self.Buffer = New DataBuffer(INFO.Length())
		Self.Buffer.PokeBytes(0, INFO.ToChars(), 0)
		
		Return Init(Parent, Self.Buffer, MSGType, Self.Buffer.Length(), DiscardBuffer)
	End
	
	Method Init:RN_Message(Parent:RN_MessageHandler, Buffer:DataBuffer, MSGType:Int, BufferLength:Int=-1, DiscardBuffer:Bool=Default_DiscardBuffer)
		If (BufferLength = -1 And Buffer <> Null) Then
			BufferLength = Buffer.Length()
		Else
			BufferLength = Max(BufferLength, 0)
		Endif
		
		Local FixByteOrder:Bool = RN_Packet.Default_FixByteOrder
		
		If (Parent <> Null) Then
			If (Parent.Parent <> Null) Then
				FixByteOrder = Parent.Parent.FixByteOrder
			Endif
		Endif
		
		Super.Construct(Buffer, 0, BufferLength, False, "", 0, FixByteOrder)
		
		Self.Parent = Parent
		Self.MessageType = MSGType
		Self.DiscardBuffer = DiscardBuffer
		
		If (Parent.VirtualIP.Length() > 0) Then
			Self.IP = Parent.VirtualIP
		Else
			Self.IP = Parent.Parent.MessageIP
		Endif
		
		If (Parent.VirtualPort > 0) Then
			Self.Port = Parent.VirtualPort
		Else
			Self.Port = Parent.Parent.MessagePort
		Endif
		
		' Return this object. (Mainly so we can use pools effectively)
		Return Self
	End
	
	' Destructor(s):	
	Method Free:RN_Message()
		If (Self.Buffer <> Null) Then
			'#Rem
			If (Self.DiscardBuffer) Then
				Self.Buffer.Discard()
			Endif
			'#End
			
			Self.Buffer = Null
		Endif
		
		Close()
		
		Self.DiscardBuffer = Default_DiscardBuffer
		Self.MessageType = 0
		'Self._Length = 0
		
		If (Self.Parent <> Null) Then
			If (Self.Parent.Parent <> Null And Self.Parent.Parent.MessageStack <> Null) Then
				Self.Parent.Parent.MessageStack.RemoveEach(Self)
			Endif
			
			'Self.Parent.MessagePool.Free(Self)
		Endif
		
		Self.Parent = Null
		Self.IP = ""
		Self.Port = 0
		
		Return Self
	End
	
	' It is recommended that you use this command instead of 'Free' in most situations:
	Method Mark:RN_Message()	
		Return Free()
	End
	
	' Properties (Public):
	
	#Rem
	Method Data:Bool(InBuffer:String) Property
		Return False
	End
	
	Method Data:Bool(InBuffer:Int[]) Property
		Return False
	End
	
	Method Data:Bool(InBuffer:DataBuffer) Property
		Return False
	End
	#End

	Method ToBuffer:DataBuffer() Property
		Return Buffer
	End
	
	' This command may be different from 'ToBuffer' in the future:
	Method Info:DataBuffer() Property
		Return Buffer
	End
	
	Method ToBytes:Int[](UseCache:Bool=False) Property
		Return ToArray(UseCache)
	End
	
	Method ToArray:Int[](UseCache:Bool=False) Property
		Local D:Int[]
		
		If (UseCache And Parent <> Null) Then
			D = Parent.RequestIntermediateCache(Buffer.Length())
		Else
			If (Buffer <> Null) Then
				D = New Int[Buffer.Length()]
			Endif
		Endif
		
		Buffer.PeekBytes(0, D, 0, Buffer.Length())
		
		' Return the data-array.
		Return D
	End
	
	Method Data:String(Encoding:String="ascii") Property
		If (Buffer <> Null) Then
			'Return String.FromChars(ToBytes(True))
			
			Return Buffer.PeekString(0, Encoding)
		Endif
		
		Return ""
	End
	
	Method ToString:String() Property
		Return Data
	End
	
	Method Type:Int() Property
		Return MessageType
	End
	
	Method Type:Void(T:Int) Property
		MessageType = T
		
		Return
	End
	
	' Properties (Private):
	Private
	
	' Nothing so far.
	
	Public
	
	' Fields (Public):
	Field Parent:RN_MessageHandler
	
	Field IP:String, Port:Int
	
	' Flags:
	Field DiscardBuffer:Bool
	
	' Fields (Private):
	Private
	
	Field MessageType:Int
	
	Public
End